\begingroup
\footnotesize
\begin{frame}[allowframebreaks]{Law of the unconscious statistician (LOTUS)}
    \gls{lotus} is a theorem employed to calculate the expected value of a function $g(X)$ of a random varible $X$ from $\f{X}$, i.e., the \gls{pdf} that is associated to $X$.
    \vfill
    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \vspace*{-\baselineskip}
            \begin{alertblock}{Case 1: $X$ is univariate and continous}
                If $X$ is a univariate continuous random variable: 
                \begin{equation*}
                    \expt{g(X)} = \int_{-\infty}^{\infty} g(x) \f{X} \, \diff{x}
                \end{equation*}
            \end{alertblock}
        \end{column}
        \begin{column}{0.5\textwidth}
            \vspace*{-\baselineskip}
            \begin{alertblock}{Case 2: $X$ is multivariate and continous}
                If $\vect{X}$ is a multivariate continuous random variable such that $\vect{X} = \left(X_1, \ldots X_m \right)^\top \in \mathbb{R}^m$:
                \begin{equation*}
                    \expt{g(\vect{X})} = \int_{-\infty}^{\infty} \cdots \int_{-\infty}^{\infty} g(\vect{X}) \f{\vect{X}} \, \diff{x_1}\ldots\diff{x_m}
                \end{equation*}
            \end{alertblock}
        \end{column}
    \end{columns}

    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \begin{alertblock}{Case 3: $X$ is univariate and discrete}
                If $X$ is a univariate discrete random variable: 
                \begin{equation*}
                    \expt{g(X)} = \sum_{x} g(x) \f{X}
                \end{equation*}
            \end{alertblock}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{alertblock}{Case 4: $X$ is multivariate and discrete}
                If $\vect{X}$ is a multivariate discrete random variable such that $\vect{X} = \left(X_1, \ldots X_m \right)^\top \in \mathbb{R}^m$:
                \begin{equation*}
                    \expt{g(\vect{X})} = \sum_{x_m} \cdots \sum_{x_1} g(\vect{X}) \f{\vect{X}}
                \end{equation*}
            \end{alertblock}
        \end{column}
    \end{columns}
\end{frame}
\endgroup 

\begingroup
\footnotesize
\begin{frame}[t]{Density Transformation Theorem}
    \begin{figure}
        \includegraphics[scale=0.8]{figures/pdf_transformation.eps}
    \end{figure}
    Given a random variable $X$ and its \gls{pdf} $\f{X}$, one can obtain the \gls{pdf} of $Y=g(X)$ if $g^{-1}\left( Y \right)$ exists.
    With the conservation of probability, we have {$A = F_Y(y_0) = F_X(g^{-1}(y_0))$}.
    \begin{equation*}
        \f{y} = \frac{\partial}{\partial y}F_Y(y) = \frac{\partial}{\partial y} F_X\left(g^{-1}\left(y\right)\right)
    \end{equation*}
    \begin{equation*}
        \boxed{\Rightarrow \f{y} = f_X\left(g^{-1}\left(y\right)\right) \left\lvert \frac{\partial}{\partial y} g^{-1}\left(y\right)\right\rvert}
    \end{equation*}
\end{frame}
\endgroup
\begin{frame}[allowframebreaks]{Derivation of Density Transformation Theorem}
    The density of functions of random variables can be calculated with the density transformation theorem.
    This theorem is essentially an extension of integration by substitution method.
    
    Let $\vect{X} = \left(X_1, \ldots X_m \right)^\top \in \mathbb{R}^m$ be an m-dimensional multivariate random variable with a joint \gls{cdf} of $\F{\vect{X}}=\prob{\vect{X}\leq \vect{x}}$ and joint \gls{pdf} of $\f{\vect{X}} = \dfrac{\partial}{\partial x_1} \cdots \dfrac{\partial}{\partial x_m} \F{\vect{X}}$.

    Assume $\vect{Y} = \left(Y_1, \ldots Y_n \right)^\top \in \mathbb{R}^n$ is  another multivariate random variable defined as a function of $\vect{X}$, 
    $\vect{Y}= \vect{G}\left( \vect{X} \right)$, where $g_i: (\mathbb{X}_1, \ldots, \mathbb{X}_m) \subset \mathbb{R}^m \mapsto \mathbb{Y}_i \subset \mathbb{R}$ is an invertible multivariate continuous function.

    By the virtue of the relation between \gls{pdf}, we can show:
    \begin{subequations}
        \begin{align}
            \int_{\mathbb{Y}_1} \cdots \int_{\mathbb{Y}_n} &\f{\vect{Y}} \, \diff{y_1} \ldots \diff{y_n} = \int_{\mathbb{X}_1} \cdots \int_{\mathbb{X}_m} \f{\vect{X}} \, \diff{x_1} \ldots \diff{x_m} = 1 \\
            \intertext{Thus, \gls{pdf} of random variable $\vect{Y}$ can be obtained as given below:}
            &\f{\vect{Y}} = \dfrac{\partial}{\partial y_1} \cdots \dfrac{\partial}{\partial y_n} \int_{\mathbb{X}_1} \cdots \int_{\mathbb{X}_m} \f{\vect{X}} \, \diff{x_1} \ldots \diff{x_m}
        \end{align}
    \end{subequations}
    On the other hand, the \gls{cdf} of random variable $\vect{Y}$, can be shown as:
    \begin{subequations}
        \begin{align}
            F_{\vect{Y}}\left( \vect{y} \right) &= \prob{\vect{Y}\leq \vect{y}} = \prob{ \vect{X} \leq \vect{G}^{-1}(\vect{y})} \\
            F_{\vect{Y}}\left( \vect{y} \right) &= \int_{\vect{s}}^{\vect{y}} \f{\vect{Y}} \, \diff{\vect{y}} = \int_{\vect{G}^{-1}(\vect{s})}^{\vect{G}^{-1}(\vect{y})} \f{\vect{X}} \, \diff{\vect{x}}
        \end{align}
    \end{subequations}
    where $\vect{s} = (\sup{\mathbb{X}_1},\ldots, \sup{\mathbb{X}_m})^\top$.

    Now substitute $\vect{x} = \vect{G}^{-1}(\vect{u})$ in the integral on the right hand side, i.e., $\vect{x} = \vect{G}^{-1 }(\vect{u})$ and $\frac{\diff{\vect{u}}}{\diff{\vect{x}}}=\vect{G}^\prime\left(\vect{x}\right)$.
    Due to the inverse function theorem, we also have $\frac{\diff{\vect{x}}}{\diff{\vect{u}}}=\frac{\diff{\vect{G}^{-1}}}{\diff{\vect{u}}}$.
    Consequentially,
    \begin{subequations}
        \begin{align}
            \int_{\vect{s}}^{\vect{y}} \f{\vect{Y}} \, \diff{\vect{y}} = \int_{\vect{s}}^{\vect{y}} f_{\vect{X}}\left(\vect{G}^{-1}(\vect{u}) \right) \, \frac{\diff{\vect{G}^{-1}}}{\diff{\vect{u}}}\diff{\vect{u}}
        \end{align}
        Taking the derivative of both sides with respect to $\vect{y}$, we have:
        \begin{align}
            \f{\vect{Y}} &=  f_{\vect{X}}\left(\vect{G}^{-1}(\vect{y}) \right) \, \frac{\diff{\vect{G}^{-1}}}{\diff{\vect{y}}} \\
            &=  f_{\vect{X}}\left(\vect{G}^{-1}(\vect{y}) \right) \lvert \det{\vect{J}_{\vect{G}^{-1}}(\vect{y})} \rvert
        \end{align}
        where $\vect{J}_{\vect{G}^{-1}}(\vect{y})$ denotes the jacobian matrix of the inverse function $\vect{G}^{-1}$ evaluated at $\vect{y}$.
        Due to the implication of inverse function theorem and the properties of determinant operator, we have $\lvert \det{\vect{J}_{\vect{G}^{-1}}(\vect{y})}\rvert = \lvert\det{\inv{\vect{J}_{\vect{G}}}(\vect{y})}\rvert = \dfrac{1}{\lvert \det{\vect{J}_{\vect{G}}(\vect{x})} \rvert}$, assuming $\det{\vect{J}_{\vect{G}}(\vect{x})} \neq 0$, i.e., $\vect{G}(\vect{x})$ is continuously differentiable.
        \begin{equation}
            \f{\vect{Y}} = \frac{f_{\vect{X}}\left(\vect{G}^{-1}(\vect{y}) \right)}{\lvert \det{\vect{J}_{\vect{G}}(\vect{x})} \rvert}
        \end{equation}
    \end{subequations}
\end{frame}


\begingroup
\footnotesize
\begin{frame}[t]{Numerical Approach}
    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            The definition of coefficient $a_k$ is given below: 
            \begin{equation*}
                a_k = \frac{1}{k!} \frac{\partial^k}{\varepsilon^n}g(\varepsilon) \f{\varepsilon}
            \end{equation*}
            where the derivative of the product can be obtained by applying the General Leibniz rule.
            \vfill
            Putting everything together, the coefficient of order $k$ is given by:
        \begin{equation*}
            \boxed{a_k = \left. \frac{g(\varepsilon)\f{\varepsilon}}{k!}\right\rvert_{\varepsilon=\mu_{\varepsilon}} + \left. \frac{(-1)^k \f{\varepsilon}}{\beta \sigma_{\varepsilon}^k}\sum\limits_{n=1}^{k} \frac{\sigma_\varepsilon^n H_{k-n}\left(\frac{\varepsilon-\mu_\varepsilon}{\sigma_\varepsilon}\right)}{n (k-n)!\, (\varepsilon+e_t)^n}\right\rvert_{\varepsilon=\mu_{\varepsilon}}.}
        \end{equation*}
        \end{column}
        \begin{column}{0.5\textwidth}
            \vspace*{-\baselineskip}
            \begin{alertblock}{Recall: General Leibniz Rule}
                \begin{equation*}
                    \frac{\partial^k}{\varepsilon^k}g(\varepsilon) \f{\varepsilon} = \sum_{n=0}^{k} \binom{k}{n} \frac{\partial^n}{\varepsilon^n}g(\varepsilon) \frac{\partial^{k-n}}{\varepsilon^{k-n}} \f{\varepsilon}
                \end{equation*}
            \end{alertblock}
            \begin{alertblock}{Derivative of $g(\varepsilon)$ and $\f{\varepsilon}$}
                \begin{equation*}
                    \frac{\partial^n}{\partial \varepsilon^n} g(\varepsilon) = 
                    \begin{cases}
                        \frac{(-1)^n (n-1)!}{\beta  \left(\varepsilon + e_t \right)^n}  & \text{for } n \geq 1 \\
                        g(\varepsilon) & \text{for } n=0
                    \end{cases}
                \end{equation*}
                \begin{equation*}
                    \frac{\partial^n}{\partial \varepsilon^n} \f{\varepsilon} = \frac{(-1)^n}{\sigma_\varepsilon^n} H_{n}\left( \frac{\varepsilon-\mu_\varepsilon}{\sigma_\varepsilon}\right) \f{\varepsilon} 
                \end{equation*}
                where
                \begin{equation*}
                    \binom{k}{n} = \frac{k!}{(k-n)!\, n!}   
                \end{equation*}
            \end{alertblock}
            \begin{alertblock}{Hermite Polynomials}
                Please note that $H_n(x)$ is the $n^{th}$ order Hermite polynomial that is often used by the statisticians to calculate the derivative of the standard normal \gls{pdf} $\varphi(x)$. 
                \begin{equation*}
                    H_{n}(x) = (-1)^n \exp{\left(\frac{x^2}{2}\right)} \frac{\partial^n}{\partial x^n} \exp{\left(\frac{-x^2}{2}\right)}
                \end{equation*}
            \end{alertblock}
        \end{column}
    \end{columns}
\end{frame}
\endgroup 



\begin{frame}{Contribution of Measurement Error in The Localization}{PDF of the Localization Error -- IV}
    The variance of the normed localization error $\sigma_{\norm{\vect{\chi}}}^2$ is given by,
    \begin{equation}
        \sigma_{\norm{\vect{\chi}}}^2 = \Var{g(\varepsilon)} = \expt{\left(g(\varepsilon)-\mu_{\norm{\vect{\chi}}}\right)^2} = \expt{g(\varepsilon)^2}- \mu_{\norm{\vect{\chi}}}^2.
        % &= \int_{0}^{\infty} \norm{\vect{\chi}}^2 \, \f{\norm{\vect{\chi}}} \,\diff \norm{\vect{\chi}} - \mu_{\norm{\vect{\chi}}}^2
    \end{equation}
    Following the procedure used in the derivation of the mean, we can obtain the variance of the normed localization errors as,
    \begin{equation}
        \begin{aligned}
            % \sigma_{\norm{\vect{\chi}}}^2 &= \expt{\norm{\vect{\chi}}^2} - \mu_{\norm{\vect{\chi}}}^2 = \expt{g(\varepsilon)^2} - \mu_{\norm{\vect{\chi}}}^2 = \int_{0}^{\infty} g(\varepsilon)^2 \, \f{\varepsilon} \,\diff{\varepsilon} \\
            % &= \int_{\mu_{\varepsilon}-2 \sigma_\varepsilon}^{\mu_{\varepsilon}+2 \sigma_\varepsilon} \sum\limits_{k=0}^{\infty} b_k (\varepsilon - \varepsilon_0)^k \,\diff{\varepsilon} \\ 
            % &= \sum\limits_{k=0}^{\infty} \frac{b_k}{k+1} (\mu_{\varepsilon}+2 \sigma_\varepsilon - \varepsilon_0)^{k+1} - \frac{b_k}{k+1} (\mu_{\varepsilon} - 2 \sigma_\varepsilon - \varepsilon_0)^{k+1} \\
            \sigma_{\norm{\vect{\chi}}}^2 &= \sum\limits_{k=0}^{\infty} \frac{b_{2k}}{2k+1} 2^{2k+2} \sigma_\varepsilon^{2k+1}
        \end{aligned}.
    \end{equation}
\end{frame}

% \input{sections/backup/evolution_intro}
% \begin{frame}[t]{Available Techniques -- 10.000 feet look}
%     \begin{columns}[T]
%         \column{0.2\textwidth}
%         \begin{figure}
%             \centering
%             \includegraphics[height=2cm,width=3cm]{example-image}
%             \caption{Visual Techniques}
%         \end{figure}

%         \column{0.2\textwidth}
%         \begin{figure}
%             \centering
%             \includegraphics[height=2cm,width=3cm]{example-image}
%             \caption{Inertial Measurements}
%         \end{figure}

%         \column{0.2\textwidth}
%         \begin{figure}
%             \centering
%             \includegraphics[height=2cm,width=3cm]{example-image}
%             \caption{Force Measurement}
%         \end{figure}

%         \column{0.2\textwidth}
%         \begin{figure}
%             \centering
%             \includegraphics[height=2cm,width=3cm]{example-image}
%             \caption{Structural Vibration}
%         \end{figure}

%         \column{0.2\textwidth}
%         \begin{figure}
%             \centering
%             \includegraphics[height=2cm,width=3cm]{example-image}
%             \caption{RADAR \& LIDAR}
%         \end{figure}
%     \end{columns}
% \end{frame}

% \begin{frame}{Spatial Features of Gait}
%     \begin{figure}
%     \centering
%     \begin{tikzpicture}[scale=.7]
%         \draw[step=0.25cm,gray,very thin, fill opacity=0.1] (-1,-1) grid (9,3); % grid
%         \only<1->{
%             \draw (0, 2) node[inner sep=0pt,anchor=west] {\includegraphics[width=.05\textwidth]{figs-quals/left_shoeprint.png}};
%         }
%         \only<2->{
%             \draw (3, 0) node[inner sep=0pt,anchor=west] {\includegraphics[width=.05\textwidth]{figs-quals/right_shoeprint.png}};
%         }
%         \only<3->{
%             \draw[densely dashed, blue] (-2, 2) -> (0, 2);
%             \draw[densely dashed, blue] (-2, 0) -> (3, 0);
%             \draw[<->, blue] (-2, 2) -- (-2, 0);
%             \draw (-2.2, 1) node [anchor=center, rotate=90, blue] {Step width};
%         }
%         \only<4->{
%             \draw[densely dashed, thick, green] (0, 2) -> (0, -2);
%             \draw[densely dashed, thick, green] (3, 0) -> (3, -2);
%             \draw[<->, green] (0, -2) -- (3, -2);
%             \draw (1.5, -2.3) node [anchor=center, green] {Step length};
%         }
%         \only<5->{
%             \draw (6, 2) node[inner sep=0pt,anchor=west] {\includegraphics[width=.05\textwidth]{figs-quals/left_shoeprint.png}};
%         }
%         \only<6->{
%             \draw[densely dashed, thick, red] (0, 2) -> (0, 4);
%             \draw[densely dashed, thick, red] (6, 2) -> (6, 4);
%             \draw[<->, red] (0, 4) -- (6, 4);
%             \draw (3, 4.2) node [anchor=center, red] {Stride length};
%         }
%     \end{tikzpicture}
%     \end{figure}
%     \begin{textblock*}{\textwidth}(10mm,40mm)
%         \begin{alertblock}<only@7>{}
%         Knowing \alert{when} and \alert{where} the feet land on the floor provides significant \alert{spatiotemporal characteristics of human gait}.
%         \end{alertblock}
%     \end{textblock*}
%     \note{}
% \end{frame}

% \begin{frame}{Temporal Stages of Human Gait}
% \note{The gait cycle is defined as the time interval between two successive
% occurrences of one of the repetitive events of walking. Although any event
% could be chosen to define the gait cycle, it is generally convenient to use the
% instant at which one foot contacts the ground (‘initial contact’). If it is decided
% to start with initial contact of the right foot, as shown in Fig. 2.1, then the
% cycle will continue until the right foot contacts the ground again. The left foot,
% of course, goes through the same series of events as the right, but displaced in time by half a cycle.}
% In order to categorically analyze the \alert{cyclic nature} of human gait, it is divided into \alert{two phases} with \alert{7 major different events}:
% \begin{figure}
%     \centering
%     \begin{tikzpicture}[scale=.95]
%         \draw[thin, dashed, ->] (12, 0) -- (13, 0);
%         \draw (12.5, -0.5) node {\footnotesize Time $t$};
%         \draw[ultra thick, red] (0, 0) -> (8, 0);
%         \draw[densely dotted, thick, red] (0, 0) |- (2.5, 1);
%         \draw[densely dotted, thick, red] (5.5, 1) -| (8, 0);
%         \draw[densely dotted, thick, green] (8, 0) |- (8.5, 1);
%         \draw[densely dotted, thick, green] (11.5, 1) -| (12, 0);
%         \draw (4, 1) node [red] {\footnotesize A. Stance Phase};
%         \draw[ultra thick, green] (8, 0) -> (12, 0);
%         \draw (10, 1) node [green] {\footnotesize B. Swing Phase};
%         % \draw[ultra thick, red] (0, -1) -> (8, -1);
%         % \draw[ultra thick, green] (8,-1) -> (12, -1);
%         \draw[thick] (0, -0.15) -> (0, 0.15);
%         \draw[thick] (2, -0.15) -> (2, 0.15);
%         \draw[thick] (4, -0.15) -> (4, 0.15);
%         \draw[thick] (6, -0.15) -> (6, 0.15);
%         \draw[thick] (8, -0.15) -> (8, 0.15);
%         \draw[thick] (10, -0.15) -> (10, 0.15);
%         \draw[thick] (12, -0.15) -> (12, 0.15);
%         % \draw[thick] (0, -1.15) -> (0, -0.85);
%         % \draw[thick] (2, -1.15) -> (2, -0.85);
%         % \draw[thick] (4, -1.15) -> (4, -0.85);
%         % \draw[thick] (6, -1.15) -> (6, -0.85);
%         % \draw[thick] (8, -1.15) -> (8, -0.85);
%         % \draw[thick] (10, -1.15) -> (10, -0.85);
%         % \draw[thick] (12, -1.15) -> (12, -0.85);
%         \draw (-1.5, 0.5) node {\footnotesize Left leg};
%         \draw (-1.5, -0.5) node {\footnotesize Right leg};
%         \draw (0, 0.5) node {\footnotesize Heel strike};
%         \draw (2, -0.5) node {\footnotesize Toe off};
%         \draw (4, 0.5) node {\footnotesize Heel rise};
%         \draw (6, -0.5) node {\footnotesize Heel strike};
%         \draw (8, 0.5) node {\footnotesize Toe off};
%         \draw (10, 0.5) node {\footnotesize Feet adjacent};
%         \draw (12, 0.5) node {\footnotesize Tibia vertical};
%     \end{tikzpicture}
%     \caption{Events and phases of human gait, adapted from (Whittle, 2014).}
%     \label{fig:gait_phases}
% \end{figure}
% \end{frame}

% \section{Simulation}
% \begin{frame}[t]{Proposed Method}
%     \input{figures/fig_process}
% \end{frame}

% \begin{frame}{Validation -- Simulation Study}
%     \begin{columns}[T]
%         \column{0.5\textwidth}
%         \begin{figure}
%             \includegraphics[width=1.1\linewidth]{figs-quals/sensorplacement.png}
%             \caption{}
%         \end{figure}
%         \column{0.5\textwidth}
%         \small
%         \justifying
%         In the following slides, the details of the simulation study is presented:
%         \pause
%         \begin{itemize}
%             \item Material and geometric properties of the floor
%             \pause
%             \item Specifications of the simulated sensors
%         \end{itemize}
%         \pause
%         This figure demonstrates the simulated floor and the sensor placement.
%         The gray shaded square represent the simulated floor plane.
%         While the blue circles and red prisms mark the location of the accelerometers and the cameras, respectively.
%     \end{columns}
% \end{frame}

% \begin{frame}{Design of Experiment}
%     \scriptsize 
%     \begin{columns}[T]
%         \column{0.4\textwidth}
%         \begin{table}[htbp]
%             \centering
%             \begin{tabular}{@{}p{3cm}c@{}}
%                 Material & Concrete (GWC) \\ \toprule
%                 Young’s Modulus & 31.490 $GPa$\\ 
%                 Dimensions & 2 $\times$ 2 $\times$ 0.025 $m$\\
%                 Density & 2443 $kg/m^3$ \\
%                 \# of Elements & 13 $\times$ 13 \\
%                 Boundary Conditions & Simply supported \\
%                 \bottomrule
%             \end{tabular}
%             \caption{Material properties used in the simulations.}
%             \label{tab:material}
%         \end{table}
%         \column{0.6\textwidth}
%         \begin{table}[htbp]
%             \centering
%             \begin{tabular}{@{} p{3cm} p{2cm} p{2cm} @{}}
%                 Cameras &  Property & Unit \\ \toprule
%                 Resolution &  1280 $\times$ 1080 & Pixel $\times$ Pixel \\ 
%                 Frame Rate (FPS) & 60 & Hertz \\
%                 Sensor Size & 8.6$\times$6.6 & mm $\times$ mm \\ 
%                 Horizontal Field-of-View & 60 & Degrees \\ 
%                 \# of Sensors & 2 \\
%                 Noise Simulated & $\sim \mathcal{N}(\vect{0}, 100~\vect{I})$ & Pixels \\\midrule
%                 Accelerometers & Property & Unit \\ \toprule
%                 Sampling Rate &  8192 & Hertz \\ 
%                 \# of Sensors & 4 \\
%                 Process Noise &  $\vect{w}_k = \vect{0}$ & $m\cdot s^{-2}$\\
%                 Measurement Noise & $\vect{v}_k = \vect{0}$ & $m\cdot s^{-2}$\\ \bottomrule
%                 \bottomrule
%             \end{tabular}
%             \caption{Specifications of the simulated sensors}
%             \label{tab:sensors}
%         \end{table}
%     \end{columns}
% \end{frame}

% \begin{frame}[t]{Mode Shapes}
%     \begin{figure}
%         \centering
%         \includegraphics[width=\textwidth]{figs-quals/mode_shapes.eps}
%     \end{figure}
% \end{frame}
% \begin{frame}[t]{Simple Results}
%         \begin{figure}
%         \centering
%         \includegraphics[width=\textwidth]{figs-quals/simulated_vibration.eps}
%     \end{figure}
% \end{frame}

% \section{Uncertainty Propagation}
% \begin{frame}{Introduction}

%     Since sensor fusion algorithm is developed in probabilistic framework, it is crucial to determine probabilistic models of each sensor modalities. \\
    
%     Dr. Al-ajlouni did uncertainty and error analysis of the vibration sensors in~\cite{Alajlouni2018}. This work is dedicated to do the same for the stereoscopic camera setup. \\
    
%     Sources of uncertainties in the optical systems can be listed as:
%         \begin{itemize}
%             \item Mis-calibration
%             \item Errors in detection
%         \end{itemize}
%     \end{frame}
    
%     \begin{frame}{Uncertainty Propagation}
%     Let $X \in \mathbb{R}^n$ be a random variable with with $\vect{\Sigma}_X$ uncertainty.
%     Given a function $\{f\}:\mathbb{R}^n \mapsto \mathbb{R}^m $ acting $X$, $\vect{\Sigma}_X$ can be projected by using the Jacobian matrix of $\vect{J_f}$:
    
%     \begin{equation}
%         \vect{\Sigma}_f  = \vect{J_f}\vect{\Sigma}_X\vect{J_f}^\top
%     \end{equation}
%     By using this theorem, the detection errors in pixel-space can be projected onto 3d space. \\
    
%     \vskip 2cm
%     \tiny{Note: If $f$ is \textbf{not} linear, then this projection can be approximated with first-order Taylor expansion of $f$.}
% \end{frame}

% \begin{frame}{Application to Perspective Transformation}

% \begin{figure}
%     \centering
%     \includegraphics[width=\textwidth]{figs-quals/projected_uncertainty.png}
%     \caption{The figure on the left shows the camera and the scene it is observing. The figure on the right shows an example detection result along with its true location. As can be seen from the figure, the detection yields some uncertainties. In this work, this uncertain detection on the image plane is described on the scene coordinate frame.}
% \end{figure}
% \end{frame}

% \begin{frame}{Projection of Uncertainties from Image Plane to World Coordinates}
%     As can be seen in \Cref{eq:pinhole}, ${}^n\vect{p}^i_k$, and ${}^n\vect{P}_k$ signify the pixel location of a joint and its world coordinates, respectively. \\ 
%     If the same transformation is used with a known ${}^n\vect{p}^i_k$, and ${}^n\vect{P}_k$ pair, the detection uncertainties can be projected onto the scene as:
    
%     \begin{equation}
%         \vect{\Sigma}_{3d} = z ^ 2 \vect{R}^\top \begin{bmatrix} \frac{1}{f_x} & 0 \\ 0 & \frac{1}{f_y} \\ 0 & 0 \end{bmatrix} \vect{\Sigma}_{2d} \begin{bmatrix} \frac{1}{f_x} & 0 & 0 \\ 0 & \frac{1}{f_y} & 0 \end{bmatrix} \vect{R}
%         \label{eq:jacpers}
%     \end{equation}
%     where $z$ is the detection distance, \\
%     $\vect{R}$ is the rotation matrix between the fixed coordinate frame and the camera,
%     $f_x$, and $f_y$ are the focal distances of the camera.
% \end{frame}

% \begin{frame}{Projection of Uncertainties from Image Plane to World Coordinates -- II}
%     As can be seen in \Cref{eq:jacpers}, when projecting the detection uncertainty from pixel space to world coordinates, the scaling has a quadratic relationship with detection distance and the inverse of the focal length. \\
    
%     In simpler terms:
%     \begin{itemize}
%         \item As a person gets far away from the camera, the detection uncertainty increases,
%         \item If people further away from the camera, camera should narrow its field-of-view.
%     \end{itemize}
% \end{frame}

% \begin{frame}{Application to Our Experimental Setup -- Camera Performance}
%     \begin{figure}
%         \centering
%         \includegraphics[width=0.6\textwidth]{figs-quals/gopro_uncertainty.png}
%         \caption{This figure shows a parametric study to analyze the effect of each distance and the detection uncertainty on the projection. Assuming the detection uncertainty is ~4 pixels, we need to put a camera for every two meters, if 10 cm uncertainty is wanted to be achieved.}
%     \end{figure}
% \end{frame}

% \begin{frame}{Application to Our Experimental Setup -- Analytical Results}
%     \begin{figure}
%         \centering
%         \includegraphics[width=0.6\textwidth]{figs-quals/image.png}
%         \caption{This figure shows a parametric study to analyze the effect of each distance and the detection uncertainty on the projection. Assuming the detection uncertainty is ~4 pixels, we need to put a camera for every two meters, if 10 cm uncertainty is wanted to be achieved.}
%     \end{figure}
% \end{frame}

\begin{frame}[t]
    \centering
    \layout
\end{frame}
