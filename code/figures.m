clc; clearvars; close all; prepare_generic;
theta = linspace(0, pi/2, 500);
h = figure('Units', 'inches', 'OuterPosition', [0 0 5 3]); hold on;
quiver(1.5, 0.5, 0, 0.5, 0, 'k-', 'Linewidth', 1.5);
quiver(1.5, 0.5, 0.5, 0, 0, 'k-', 'Linewidth', 1.5);
quiver(0, 0, 0, 1, 0, 'k-', 'Linewidth', 1.5);
quiver(0, 0, 1, 0, 0, 'k-', 'Linewidth', 1.5);
error_ellipse('C', 0.01 * [1 0.4; 0.4 1], 'mu', [0.5 1], 'conf', 0.95, 'style', "r-.")
scatter(0.5, 1, 'r+');
quiver(1.5, 0.5, -0.9, 0.6, 0, 'r-.', 'Linewidth', 1); % estimation
quiver(0, 0, 1.5, 0.5, 0, 'k-', 'Linewidth', 1); % xai
quiver(0, 0, 0.4, 1, 0, 'g-', 'Linewidth', 1); % xtrue
% quiver(0.4, 1, 0.2, 0.1, 0, 'b-.', 'Linewidth', 1); % chi

text(0.24, 0.5, '\boldmath$\mathrm{x}_{true}$')
text(0.75, 0.2, '\boldmath$\mathrm{x}_{a_i}$')
text(0.4, 1.3, '\boldmath$\mathrm{\chi}_i$')

text(2.0, 0.4, '$x_{a_i}$')
text(2.0, 0.4, '$x_{a_i}$')
text(1.3, 0.95, '$y_{a_i}$')
text(1, -0.05,'$x$')
text(-.15, 0.95, '$y$')
text(0.65, 0.7, '$g(e_i; \beta_i)$')
axis equal; grid off; axis off;
% legend("Estimated Location {\boldmath$\mathrm{x}_i$}", ...
%        "Impact Location {\boldmath$\mathrm{x}_t$}", ...
%        "$95\%$ Confidence Interval")
% xlim([0 1]); ylim([0 1]);
% xticks([0.61]); xticklabels(['$d_i = g_d(z_i; \beta_d)$'])
xlabel('x [meter]'); ylabel('y [meter]')
exportgraphics(gcf, '../manuscript/figs/layout.eps', 'BackgroundColor', 'none', 'Resolution', 600)

% h = figure('OuterPosition', [0 0 250 250], 'Units', 'pixels'); hold on;
% d = linspace(0, 1, 100);
% grid on; grid minor;
% plot(d, normpdf(d, 0.6, 0.1));
% xlim([0, 1])
% xticks([0.61]); xticklabels(['$d_i = g_d(z_i; \beta_d)$'])
% xlabel('$d_i$ [meter]'); ylabel('Probability $f_{d_i}(d_i)$')
% exportgraphics(gcf, '../torgersen-award-manuscript/fd.eps', 'BackgroundColor', 'none', 'Resolution', 600)


% h = figure('OuterPosition', [0 0 250 250], 'Units', 'pixels'); hold on;
% theta = linspace(0, 2*pi, 100);
% grid on; grid minor;
% plot(theta, 1/2/pi * ones(size(theta)));
% xticks([0, 2*pi]); xticklabels({'0', '2$\pi$'})
% yticks([0, 1/2/pi]); yticklabels({'0', '$\frac{1}{2\pi}$'})

% xlabel('$\theta_i$ [radian]'); ylabel('Probability $f_{\theta_i}(\theta_i)$');
% ylim([0, 0.3]);
% exportgraphics(gcf, '../torgersen-award-manuscript/ftheta.eps', 'BackgroundColor', 'none', 'Resolution', 600)

% nrepeat = 5000;
% seed = ceil(time().Second);
% rng(seed)
% t = linspace(0, 10, 1000);
% x = exp(-1*(t)) .* sin(5*(t));
% y = x;
% noise = randn(nrepeat, size(t, 2)) * 0.06;
% signal = y + noise;
% energy(signal, 1);
% h = figure('OuterPosition', [0 0 300 250], 'Units', 'pixels'); hold on;
% fill([t, fliplr(t)], [y + 2 * 0.06,  fliplr(y - 2 * 0.06)], [0.8 0.8 0.8], 'LineStyle', 'none')
% plot(t, signal(1, :),  'r-');
% plot(t, x,  'k-.', 'Linewidth', 2);
% grid on; grid minor;
% xlabel("Time [secs]");
% ylabel("Amplitude [Volts]")
% legend("$95\%$ CI", "Measurement $z_i[k]$", "True measurement $z_{t,i}[k]$")
% exportgraphics(gcf, '../torgersen-award-manuscript/signal.eps', 'BackgroundColor', 'none', 'Resolution', 600)
