function [h] = propagation_figure(sim, step_idx)
    ei = squeeze(sim.signals.sensor.energy);
    ei_dbm = 10 * log10(ei);
    eti = sim.signals.raw.energy;
    eti_dbm = 10 * log10(eti);
    dti = pdist2(sim.impact_locations, sim.sensor_locations);

    q = repmat(dti, 1, 1, sim.nrepeat); q = permute(q, [3, 1, 2]);
    h = figure;
    plot(q(:), ei(:), '.');
    % yyaxis right;
    % plot(dti(:), eti_dbm(:), 'x');
    grid on; grid minor;
    legend(["$e_i$", "$e_{i, t}$"])
    xlabel("Distance $d$ [m]");
    ylabel("Registered Signal Energy [db]");
end

