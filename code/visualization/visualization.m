% h = propagation_figure(sim); saveas(h, 'figures/propagation.eps', 'epsc'); close(h);
% parfor k=1:sim.nlocations
%     h = signal_energy_spatial(sim, k);
%     saveas(h, 'figures/energy_spatial.eps', 'epsc');
%     close(h);
% end
% 
parfor k=1:sim.nsamples
    h = floor_vibration_timeseries(sim, k); axis tight; drawnow;
    saveas(h, sprintf('figures/displacement/energy_spatial-%06d.png', k));
    close(h);
end
% 
for k=1:20
    h = plot_mode_shape(sim, k); axis tight; 
    saveas(h, sprintf('figures/modal/mode-shape-%02d.eps', k), 'epsc');
    close(h);
end
