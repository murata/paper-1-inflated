
figure('Units', 'inches', 'InnerPosition', [0 0 3 2.5]);
violinplot(normed_errors, names, "ShowNotches", false, ...
          "MedianMarkerSize", 12, "QuartileStyle", "shadow", "DataStyle", "none");
grid on; grid minor; ylabel("Error [m]"); title("Normed Localization Error");
pause(0.5);
exportgraphics(gcf, "../manuscript/figs/occupant-2/violinplot.eps");

figure('Units', 'inches', 'InnerPosition', [0 0 3 3]);
boxplot(normed_errors, names); grid on; grid minor; ylabel("Error [m]"); title("Normed Localization Error");
ylim([-5 15]);
exportgraphics(gcf, "../manuscript/figs/occupant-2/boxplot.eps");
 
figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
plot(e_space, pdfs(1, :), "LineWidth", 2);
plot(e_space, pdfs(2, :), "LineWidth", 2);
plot(e_space, pdfs(3, :), "LineWidth", 2);
grid on; grid minor;
xlabel("Localization Error [m]"); ylabel("Frequency"); title("Empirical PDF of Localization Error");
legend("Baseline", "Proposed-1", "Proposed-2");
ylim([0, 1]);
exportgraphics(gcf, '../manuscript/figs/occupant-2/epdf.eps');

figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
plot(e_space, cdfs(1, :), "LineWidth", 2);
plot(e_space, cdfs(2, :), "LineWidth", 2);
plot(e_space, cdfs(3, :), "LineWidth", 2);
grid on; grid minor;
xlabel("Localization Error [m]"); ylabel("Probability"); title("Empirical CDF of Localization Error");
legend("Baseline", "Proposed-1", "Proposed-2", "Location", "southeast"); 
exportgraphics(gcf, '../manuscript/figs/occupant-2/ecdf.eps');

figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
scatter(experiment.groundtruth(1, :), results_all(:, 1, 1), '.');
scatter(experiment.groundtruth(1, :), results_all(:, 1, 2), 'x');
scatter(experiment.groundtruth(1, :), results_all(:, 1, 3), '+');
plot(5:0.1:40, 5:0.1:40, 'k-.')
grid on; grid minor; axis equal;
xlim([5, 40]); ylim([5, 40]);
xlabel("True $x$"); ylabel("Measured $x$"); 
legend("Baseline", "Proposed-1", "Proposed-2")
exportgraphics(gcf, '../manuscript/figs/occupant-2/proposed-x.eps');

figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
scatter(experiment.groundtruth(2, :), results_all(:, 2, 1), '.');
scatter(experiment.groundtruth(2, :), results_all(:, 2, 2), 'x');
scatter(experiment.groundtruth(2, :), results_all(:, 2, 3), '+');
plot(29:0.1:35, 29:0.1:35, 'k-.')
grid on; grid minor; axis equal;
xlim([29, 35]); ylim([29, 35])
xlabel("True $y$"); ylabel("Measured $y$"); 
legend("Baseline", "Proposed-1", "Proposed-2")
exportgraphics(gcf, '../manuscript/figs/occupant-2/proposed-y.eps');

figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
scatter(1:162, joint_information(1, :), 'kx');
plot([1:162; 1:162], [min(squeeze(average_information(1, :, :)));  max(squeeze(average_information(1, :, :)))], "b-");
plot(1:162, mean(squeeze(average_information(1, :, :))), "r.");
grid on; grid minor; xlabel("Step Index"); ylabel("Entropy [Shannon]"); title("Observed Entropy");
xlim([1, 162]); ylim([-10, 7]);
exportgraphics(gcf, '../manuscript/figs/occupant-2/average_information-proposed-1.eps');


figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
scatter(1:162, joint_information(2, :), 'kx');
plot([1:162; 1:162], [min(squeeze(average_information(2, :, :)));  max(squeeze(average_information(2, :, :)))], "b-");
plot(1:162, mean(squeeze(average_information(2, :, :))), "r.");
grid on; grid minor; xlabel("Step Index"); ylabel("Entropy [Shannon]"); title("Observed Entropy");
xlim([1, 162]); ylim([-10, 7]);
exportgraphics(gcf, '../manuscript/figs/occupant-2/average_information-proposed-2.eps');

figure('Units', 'inches', 'InnerPosition', [0 0 3 3]); hold on;
scatter(joint_information(1, :), normed_errors(:, 1), 'rx'); 
scatter(joint_information(2, :), normed_errors(:, 2), 'k.'); 
% axis equal;
axis tight;
grid on; grid minor;
xlim([-10, 0]); ylim([0, 20])
xlabel('Entropy [Shannon]');
ylabel('Normed Localization Error [m]')
set(gca, 'XDir', 'reverse');
legend("Proposed-1", "Proposed-2")
exportgraphics(gcf, '../manuscript/figs/occupant-2/entropy-error.eps');
close all;
