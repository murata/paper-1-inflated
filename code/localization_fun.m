function d = localization_fun(e, beta)
    a = beta(1);
    b = beta(2);
    d = a * e .^ b;
end
