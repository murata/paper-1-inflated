clearvars; close all; clc; prepare_generic; warning off;
load('../data/AI_loc_Subject1SD_8192Hz_2to500Hz.mat');
% load("../data/AI_loc_Subject2Rodri_8192Hz_2to500Hz.mat");
load("../data/GroundTruth_SensorCoordinates_DetectionTime.mat");
load("../data/goodwin_step_data.mat");

d_true = pdist2(trueFootstepLocations', sensorCoordinates');
nsamples = size(data, 1);
e_k = cumsum(data.^2, 1);
t = 1:nsamples;
fs = 8192;
t = t / fs;
nwindow = fs*2;
selection_map = d_true < 4;
observed_steps = sum(selection_map, 2);
%%
e = zeros(162, 11);
nsamples = zeros(162, 1);
ek = cumsum(data.^2, 1)';
tf = zeros(11, length(t));
ft = nan(11, 162*2, length(t));
for sensor_idx=1:11
    tf(sensor_idx, :) = ischange(ek(sensor_idx, :), 2);
    tf_idx = find(tf(sensor_idx, :));
    fprintf("Sensor ID: %d Total Candidates: %d\n", sensor_idx, length(tf_idx))
    parfor i = 1:length(tf_idx)
        ft(sensor_idx, i, :) = normpdf(t, t(tf_idx(i))-0.2, 0.08);
    end
end
ft_sensor = squeeze(sum(ft, 1, 'omitnan'));
ft_t = sum(ft_sensor);

t_crit = islocalmax(ft_t);
t_crit_idx = find(t_crit);
fprintf("Number of Events Found: %d\n", length(t_crit_idx));
%%

figure(1);
set(gcf,'renderer','painters', 'visible', 'off');

parfor idx=1:length(t_crit_idx)
    fprintf("Exporting event figures. Event ID: %d/%d\n", idx, length(t_crit_idx));
    try
        t_start = t_crit_idx(idx);
        t_end = t_crit_idx(idx+1);
    catch
        t_start = t_crit_idx(idx);
        t_end = length(data);
    end
    window = data(t_start:t_end, :);
    e(idx, :) = energy(window, 1);
    nsamples(idx) = length(window);
    fname = sprintf("../figures/temporal_marking/subject-2/%d.png", idx);
    subplot(211); plot(t(t_start:t_end), ft_t(t_start:t_end)); grid on; grid minor; ylim([min(ft_t), max(ft_t)]);
    xlim([t(t_start), t(t_end)]);
    subplot(212); plot(t(t_start:t_end), data(t_start:t_end, :)); grid on; grid minor;
    ylim([min(data(:)), max(data(:))]);
    xlim([t(t_start), t(t_end)]);
    drawnow;
    exportgraphics(gcf, fname, 'Resolution', 300);
end
close(gcf);

save('../data/automatic_detection_energy_subject_1.mat', 'e', 'nsamples');
