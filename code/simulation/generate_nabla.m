function [f, del] = generate_nabla(M, lambda)
    syms x_i y_i e_i beta_i [M, 1] real;
    syms beta_0 x y real;

    d = sym(zeros(M, 1));
    for i=1:M
        d(i) = norm([x, y] - [x_i(i), y_i(i)]);
    end

    r = beta_0 * d + beta_i - e_i;
    % p = [x; y];
    p = [x; y; beta_0; beta_i(:)];
    term1 = r / norm(r);
    q = p(1:2) - [1; 1];

    eq1 = norm(r);
    eq2 = (1-lambda) * norm(r) + (lambda) * norm(q);

    % f(p) = piecewise(norm(q) == 0, eq1, norm(q) > 0, eq2);
    f(p) = eq1;
    del(p) = gradient(f, p);
end
