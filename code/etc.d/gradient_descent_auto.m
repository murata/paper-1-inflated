function [x_hat, fval] = gradient_descent(XY_t, XY_i, e, gamma, e_0, sigma_z)
    options = optimoptions('fminunc', ...
                           'Display', 'iter', ...
                           'Algorithm', 'quasi-newton', ...
                           'HessUpdate', 'steepdesc', ...
                           'SpecifyObjectiveGradient', false);
%                            'Algorithm','trust-region', ...
    % optimize the objective function
    [x_hat,fval] = fminunc(@cost, e, options);
    % define the objective function
    function [f, g] = cost(e)
        m = length(e);
        q = zeros(m, 1);
        d_hat = zeros(m, 1);
        for i=1:m
            d_hat(i) = PiD_inv(e(i), gamma, e_0);
            q(i) = -1/2/pi/d_hat(i)*PiD_prime(d_hat(i), gamma, e_0)*normpdf(PiD(d_hat(i), gamma,e_0), e(i), 1);
        end
%         scatter(1:m, q);
%         drawnow;
        f = log(abs(prod(q)));
        g = ones(m, 1);
    end
end