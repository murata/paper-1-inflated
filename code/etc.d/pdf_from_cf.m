function fX = pdf_from_cf(x,phiX,domain)
    t = sym('t');
    g(t) = exp(-1i*t*x)*phiX;
    if numel(domain) == 2
        fX = 1/(2*sym('pi'))*int(g, t, domain(1), domain(2));
    else
        fX = 1/(2*sym('pi'))*int(g, t);
    end
end