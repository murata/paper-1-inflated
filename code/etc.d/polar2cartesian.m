function [pxy] = polar2cartesian(pd, d_space, ...
    ptheta, theta_space, ...
    xy, nx, ny, xs, x, y)
    pxy = zeros(ny, nx);
    abuzittin = zeros(length(xy), 6);
    for qq = 1:length(xy)
        [I,J] = ind2sub([ny, nx], qq);
        xy_i = xy(qq, :);
        dxy = xy_i - xs;
        d = vecnorm(dxy);
        theta = atan2(dxy(2), dxy(1));
        [~, theta_id] = min(abs(theta-theta_space));
        [~, d_id] = min(abs(d-d_space));
        pxy(I, J) = pd(d_id) * ptheta(theta_id) / d;
    end
    
    scaler = trapz(x, trapz(y, pxy));

    if isfinite(scaler) && scaler ~= 0
        pxy = pxy / scaler;
        % figure(666); mesh(x, y, pxy); colorbar; hold on;
        % plot3(xs(:, 1), xs(:, 2), 100, 'gx'); hold off; drawnow;
        % colormap jet; lighting phong; shading interp; axis equal; view([0 90]); drawnow;
    end
end

