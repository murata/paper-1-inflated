function [mode_shapes, mode_frequencies] = plate_mode_shapes(K, M)
    [UU,SS]=eig(K, M, 'vector');
    SS=sqrt(SS)/(2*pi); %undamped natural frequencies
    [mode_frequencies,I]=sort(SS);
    mode_shapes=UU(:,I);
end

