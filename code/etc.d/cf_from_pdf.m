function cf = cf_from_pdf(x,fX,domain)
    t = sym('t');
    g(x) = fX*(cos(t*x)+1j*sin(t*x));
    if numel(domain)==2
        cf(t) = int(g, x, domain(1), domain(2));
    else
        cf(t) = int(g, x);
    end
end