function fx = mvn_pdf(x, mean, cov, c)
    k = length(mean);
    den = sqrt((2*c)^k * det(cov));
    num = exp(-0.5* (x-mean)'*inv(cov)*(x-mean));
    fx = num/den;
end