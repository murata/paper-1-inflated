function y = psi_huber(x, b)
    if abs(x) > b
        y = b*sign(x);
    else
        y = x;
    end
end

