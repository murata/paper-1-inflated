function now = time(varargin)
    if nargin>0
        fmt = varargin{1};
    else
        fmt = "MM-dd hh:mm:ss.SSS";
    now = datetime("now", "Format", fmt);
end
