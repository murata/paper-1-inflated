clc; clearvars; close all; prepare_generic;
load("../data/goodwin_step_data.mat");
load("../data/automatic_detection_energy_subject_2.mat");
load('../data/grad.mat');

experiment = experiment2;
names = ["Baseline", "Proposed-1", "Proposed-2"];

nx = 900; ny = 300;
space = generate_space(13, 31, 27, 33, nx, ny);
d_true = pdist2(experiment.sensor_locations', experiment.groundtruth');
xy = [space.xx(:), space.yy(:)];
options = optimset('Display', 'off');

nsensors_used = 11;
nsteps = 162;
ncases_total = 2^nsensors_used - 1;
errors = zeros(3, nsteps, ncases_total, 2);
normed_errors = zeros(3, nsteps, ncases_total);
joint_information = zeros(3, nsteps, ncases_total);
average_information = zeros(3, nsteps, nsensors_used);
results_all = zeros(3, nsteps, ncases_total, 2); % techniques x steps x cases x ndim
errors_all = zeros(3, nsteps, ncases_total, 2);
cases = zeros(ncases_total, nsensors_used);

for i=1:ncases_total
    bstr = dec2bin(i, nsensors_used);
    cases(i, :) = str2num(reshape(bstr',[],1));
end
nsensors_total = sum(cases, 2);
fit_curves;
duration = zeros(nsteps, 1);
code_start = time();
kappas = zeros(nsteps, nsensors_used);
for step_idx=1:nsteps
    parfor sensor_idx = 1:nsensors_used
        for algorithm = 2:3
            if (algorithm == 2)
                fd_model = "uniform";
            else
                fd_model = "gaussian";
            end
            fun = @(x)problem(x, step_idx, sensor_idx, experiment, e, models{sensor_idx}, nx, ny, nsamples, xy, space, fd_model, grad);
            kappas(step_idx, sensor_idx) = fminbnd(fun, 0, 1, options);
        end
    end
    step_start = time();
    parfor case_idx=1:ncases_total
        now = time();
        template = cases(case_idx, :);
        sensor_idx = find(template);
        nsensors = length(sensor_idx);

        if nsensors == 1
            continue;
        end

        for algorithm = 1:3
            if algorithm == 1
                L = experiment.sensor_locations(:, sensor_idx);
                ee = e(step_idx, sensor_idx);
                estimate = L * ee' / sum(ee);
            else
                if (algorithm == 2)
                    fd_model = "uniform";
                else
                    fd_model = "gaussian";
                end
                fxy = zeros(ny, nx, nsensors);
                % for qq = 1:nsensors
                %     sensor_idx_idx = sensor_idx(qq);
                %     fun = @(x)problem(x, step_idx, sensor_idx_idx, experiment, e, models{sensor_idx_idx}, nx, ny, nsamples, xy, space, fd_model, grad);
                %     x = fminbnd(fun, 0, 1, options)
                %     [~, fx] = fun(x);
                %     fxy(:, :, qq) = fx;
                % end
                for qq = 1:nsensors
                    sensor_idx_idx = sensor_idx(qq);
                    fun = @(x)problem(x, step_idx, sensor_idx_idx, experiment, e, models{sensor_idx_idx}, nx, ny, nsamples, xy, space, fd_model, grad);
                    [~, fx] = fun(kappas(step_idx, sensor_idx_idx));
                    fxy(:, :, qq) = fx;
                end

                joint = prod(fxy, 3);
                division = trapz(space.x, trapz(space.y, joint));
                joint_pdf = joint / (division+eps);
                joint_info = -joint_pdf .* log2(joint_pdf);
                joint_info(isnan(joint_info)) = 0;

                joint_information(algorithm, step_idx, case_idx) = trapz(space.x, trapz(space.y, joint_info));

                [C,I] = max(joint(:));
                % if sum(joint(:)==C) > 1
                %     return
                % end
                [Ji, Ii] = ind2sub(size(joint), I);
                estimate =  [space.x(Ii); space.y(Ji)];
                % Mean or Mode?
                % mu_x = trapz(space.x, trapz(space.y, space.x .* joint_pdf));
                % mu_y = trapz(space.x, trapz(space.y, space.y' .* joint_pdf));
                %% Plots for the debugging
                % figure(1); clf; contour(space.x, space.y, joint, 50); hold on;
                % scatter(estimate(1), estimate(2), 'rx');
                % scatter(mu_x, mu_y, 'kx');
                % scatter(experiment.groundtruth(1, step_idx), experiment.groundtruth(2, step_idx), 'g.');
                % scatter(experiment.sensor_locations(1, sensor_idx), experiment.sensor_locations(2, sensor_idx), 'bo');
                % axis equal; axis tight; drawnow limitrate;
            end
            results_all(algorithm, step_idx, case_idx, :) = estimate;
            errors_all(algorithm, step_idx, case_idx, :) = estimate - experiment.groundtruth(:, step_idx);
        end

        if mod(case_idx, 10) == 0
            fprintf("Step ID: %3d, Case ID: %4d, nsensors: %3d Duration: %1.3f\n", step_idx, case_idx, nsensors, seconds(time()- now));
            now = time();
        end

        if mode(case_idx, 200) == 0
            save('results_all.mat', '-v7.3');
        end
    end
    qq = squeeze(vecnorm(errors_all(:, step_idx, :, :), 2, 4));
    for i=2:nsensors_used
        for j=1:3
            case_idx = find(nsensors_total==i);
            % sensor_idx = find(cases(case_idx, :)==1);
            this_errors = qq(j, case_idx);
            desc = describe(this_errors(:));
            fprintf("Processing: %10s nsensors: %3d Mean: %1.2f Std: %1.2f Med: %1.2f RMS: %1.2f Min: %1.2f Max:%1.2f\n", names(j), i, desc);
        end
    end
    duration(step_idx) = seconds(time()-step_start);
    mean_duration = sum(duration) / nnz(duration);
    ncompleted = nnz(duration) / numel(duration);
    fprintf("Step Idx: %3d Duration: %3.3f [secs] Completed: %2.2f ETA: %10s\n", step_idx, duration(step_idx), 100*ncompleted, code_start + seconds(nsteps * mean_duration));

end

normed_errors = vecnorm(errors_all, 2, 4);
for i=2:nsensors_used
    for j=1:3
        case_idx = find(nsensors_total==i);
        % sensor_idx = find(cases(case_idx, :)==1);
        this_errors = normed_errors(j, :, case_idx);
        desc = describe(this_errors(:));
        fprintf("Processing: %10s nsensors: %3d Mean: %1.2f Std: %1.2f Med: %1.2f RMS: %1.2f Min: %1.2f Max:%1.2f\n", names(j), i, desc);
    end
end

save('results_all.mat', '-v7.3');
function [f, varargout] = problem(kappa, step_idx, sensor_idx, experiment, e, model, nx, ny, nsamples, xy, space, fd_model, grad)
    xi = experiment.sensor_locations(:, sensor_idx)';
    delta = xi - experiment.groundtruth(:, step_idx)';
    [~, qp] = min(abs(space.x - experiment.groundtruth(1, step_idx)));
    [~, qq] = min(abs(space.y - experiment.groundtruth(2, step_idx)));

    beta = [model.a, model.b, atan2(delta(2), delta(1))];
    prob = pdf_fx(xy, xi, e(step_idx, sensor_idx)*kappa, nsamples(step_idx), experiment.sigma_zeta, beta, fd_model);
    qwert = reshape(prob, [ny, nx]);
    division = trapz(space.x, trapz(space.y, qwert));
    fxy = qwert / (division + eps);
    result = [space.x(qp), space.y(qq)];
    f = -log(fxy(qq, qp));
    % j = subs(grad, {"x", "y", "a", "b", "e_i", "k", "n", "sigma_i", "x_i", "y_i"}, ...
    %                {result(1), result(2), model.a, model.b, e(step_idx, sensor_idx), kappa, nsamples(step_idx), experiment.sigma_zeta, experiment.sensor_locations(1, sensor_idx), experiment.sensor_locations(2, sensor_idx)});
    % j = double(j);
    if isinf(f)
        f = 999999999999;
    end
    nout = max(nargout,1) - 1;
    for k = 1:nout
        varargout{k} = fxy;
    end

end
