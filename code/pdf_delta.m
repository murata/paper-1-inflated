function fdelta = pdf_delta(delta, dt, beta, fE)
    ginv = @(e, beta) beta(0) * e ^ beta(1);
    d_ginv = @(e, beta) ginv(e, beta) / (beta(1) * e);
    e_hat = ginv(delta + dt, beta);
    fdelta = fE(e_hat) * abs(d_ginv(de, beta)); 
end
