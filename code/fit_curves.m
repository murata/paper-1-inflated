ee = e(:);
dd = d_true(:);
models = {};
residuals = zeros(11, 162);
for sensor_idx=1:experiment.nsensors
    [model, gof] = fit(e(:, sensor_idx), d_true(sensor_idx, :)', 'power1', 'Robust', 'LAR');
    % 'Lower', [0, -inf], 'Upper', [inf, 0]
    models{sensor_idx} = model;
    residuals(sensor_idx, :) = feval(model, e(:, sensor_idx)')' - d_true(sensor_idx, :);
end
% figure(666);
% for sensor_idx=1:experiment.nsensors
%     ax(sensor_idx) = subplot(3, 4, sensor_idx);
%     scatter(d_true(sensor_idx, :), abs(residuals(sensor_idx, :)), 'k.');
%     grid on; grid minor;
%     axis equal; axis tight;
% end
% linkaxes(ax, 'xy');

% figure(667);
% for sensor_idx=1:experiment.nsensors
%     ax(sensor_idx) = subplot(3, 4, sensor_idx);
%     scatter(d_true(sensor_idx, :), residuals(sensor_idx, :), 'k.');
%     grid on; grid minor;
%     axis equal; axis tight;
% end
% linkaxes(ax, 'xy');

