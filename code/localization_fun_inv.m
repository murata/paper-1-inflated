function e = localization_fun_inv(d, beta)
    e = (d/beta(1)) .^ (1/beta(2));
end
