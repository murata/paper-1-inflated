\documentclass[final]{beamer}
\usepackage{ambarkutuk-paper}
\usepackage[scale=1.24]{beamerposter} % Use the beamerposter package for laying out the poster

\usetheme{confposter} % Use the confposter theme supplied with this template

\setbeamercolor{block title}{fg=chicago_maroon,bg=White} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
% \setbeamercolor{block alerted title}{fg=white,bg=dblue!70} % Colors of the highlighted block titles
% \setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty
\newenvironment{variableblock}[3]{%
  \setbeamercolor{block body}{#2}
  \setbeamercolor{block title}{#3}
  \begin{block}{#1}}{\end{block}}
%-----------------------------------------------------------
% Define the column widths and overall poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% In this template, the separation width chosen is 0.024 of the paper width and a 4-column layout
% onecolwid should therefore be (1-(# of columns+1)*sepwid)/# of columns e.g. (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be (2*onecolwid)+sepwid = 0.464
% Set threecolwid to be (3*onecolwid)+2*sepwid = 0.708

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\newlength{\threecolwid}
\setlength{\paperwidth}{48in} % A0 width: 46.8in
\setlength{\paperheight}{36in} % A0 height: 33.1in
\setlength{\sepwid}{0.024\paperwidth} % Separation width (white space) between columns
\setlength{\onecolwid}{0.22\paperwidth} % Width of one column
\setlength{\twocolwid}{0.464\paperwidth} % Width of two columns
\setlength{\threecolwid}{0.708\paperwidth} % Width of three columns
\setlength{\topmargin}{-0.5in} % Reduce the top margin size

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{An Occupant Localization based on Uncertain Vibro-measurements} % Poster title

\author{Murat Ambarkutuk} % Author(s)

\institute{The Bradley Department of Electrical and Computer Engineering} % Institution(s)

%----------------------------------------------------------------------------------------
\input{definitions}
\input{glossaries}
\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

\setlength{\belowcaptionskip}{2ex} % White space under figures
\setlength\belowdisplayshortskip{2ex} % White space under equations

\begin{frame}[t] % The whole poster is enclosed in one beamer frame
    \vspace{-5ex}
    \begin{columns}[t]
        \begin{column}{\sepwid}\end{column} % Empty spacer column

        \begin{column}{\onecolwid} % The first column
            \begin{block}{Introduction}
                Existing methods for solving the \alert{indoor occupant location problem} with \alert{passive perception} schemes (determining, for example, the location of thefootsteps of a person without an active emitter walking across a room) suffer from high uncertainty.
                In my work, I present a \alert{novel probabilistic localization framework} that tackles both \alert{model imperfections} and \alert{measurement uncertainty} to improve the precision of computed localization estimates.
            \end{block}  
            \vspace{-2ex}
            \begin{block}{Materials \& Methods}
                \begin{enumerate}
                    \item When an occupant takes a step, the \alert{footfall pattern} of the occupant applies some forcing, i.e., the ground reaction force, \alert{on the floor} which generates \alert{structural vibrations wave} in it.
                    \item This \alert{wave is then sensed by $m$ number of accelerometers} yielding to a $n-$dimensional vibro-measurement vector $\vect{z}_i = (z_i[1], \ldots, z_i[n])^\top \in \mathbb{R}^n$ for all sensors with $i$ where $\forall i \in \{1,\ldots, m\}$. It is well-known in the literature that the vibro-measurements are often \alert{disturbed with random measurement errors} and \alert{drifted due to sensor bias}.
                    \item This work employs the \alert{\gls{pdf}} $\f{\vect{Z}_i}$ of measurement vector $\vect{z}_i$ that assigns a probabilistic measure to denote how likely to obtain the measurement \alert{to account for these uncertainty sources}.
                    \vspace{3ex}
                    \begin{alertblock}{Localization Framework $\vect{h}_i(\cdot)$}
                        \small
                        The equation below summarizes the localization framework $\vect{h}_i(\cdot, \vect{\beta}): \vect{z}_i \mapsto \vect{x}_i$ where the known calibration vector $\vect{\beta}$ represents some parameters describing the characteristics of the wave propagation phenomenon occuring in the floor.
                        \begin{equation*}
                            \boxed{
                                \vect{x}_i = \vect{h}_i(\vect{z}_i; \vect{\beta})
                            }
                        \end{equation*}
                        % With the given representation, the location estimate $\vect{x}_i$ with its corresponding localization error $\vect{\chi}_i$ are given by when the occupant's true heel-strike location is $\vect{x}_{true}$.
                        The proposed framework produces another set of \glspl{pdf} denoting each sensor's belief about occupant location $\f{\vect{X}_i}$.
                        These \glspl{pdf} are combined within a Sensor Fusion algorithm to obtain the consensus among the sensors' beliefs, i.e., the joint \glspl{pdf} $\f{\vect{X_1}, \ldots, \vect{X}_m}$.
                    \end{alertblock}
                \end{enumerate}

                % In essence, \glspl{pdf} $f_{\vect{X}_i}\left(\vect{x} \right)$ inherently assign a probability to any arbitrary location vector $\vect{x}$ in the localization space forming sensors' beliefs about the occupant location.
             \end{block}
        \end{column} % End of the first column

        \begin{column}{\sepwid}\end{column} % Empty spacer column
        \begin{column}{\twocolwid} % Begin a column which is two columns wide (column 2)

            \begin{alertblock}{Framework Overview}

            \input{figures/fig_system.tex}

            \end{alertblock} 
\begin{columns}[t,totalwidth=\twocolwid] % Split up the two columns wide column again
\begin{column}{\onecolwid}
    \vspace{-5ex}
    \begin{block}{Results}
        Experiments were done in \alert{Goodwin Hall} ($4^{th}$ floor, south hallway) which is equipped with \alert{200+ accelerometers} embedded in its superstructure.
        The occupant was asked to walk along the hallway while closest \alert{11 under-floor accelerometers} were used to capture the vibro-measurements of these 162 steps.
        \begin{figure}[!t]
            \centering
                \includegraphics[width=0.8\textwidth]{step_47.png}
                \caption{Marginal \glspl{pdf} of the occupant (marked with {\color{black}{+}}) by the sensors (marked with {\color{red}{x}})}
        \end{figure}
        \vspace{-3ex}
        \begin{figure}[!t]
            \centering
                \includegraphics[width=0.8\textwidth]{joint_47.png}
                \caption{Joint-\gls{pdf} of the measurement.}
        \end{figure}
        \vspace{-3ex}
        \begin{figure}[!t]
            \includegraphics[width=0.8\textwidth]{error_function.png}
            \caption{Localization error as a function of number of sensors used within the framework}
            \label{fig:error_function}
        \end{figure}
    \end{block}
\end{column}
\begin{column}{\onecolwid} % The second column within column 2 (column 2.2)
    \vspace{-5ex}
    \begin{block}{Discussions}
        \begin{enumerate}
            \item With \alert{the controlled experiments}, we observed that the proposed framework yielded \gls{pdf}s that represents \alert{the occupant location accurately}.
            \item To handle the uncertainty of the estimations, multi-sensor perception scheme was employed which resulted in a \alert{significant increase} in the \alert{information about the occupant location}.
            \item We can summarize our conclusions with a counter-intuitive statement: \alert{``Engineering principles suggest replacing the sub-standard sensors with better ones, and we suggest the employment of many of these sensors.''}
        \end{enumerate}
    \end{block}
    \vspace{-2ex}
    \begin{block}{Interested in Learning More?}
        \vspace{-1ex}
        \centering
        \includegraphics[width=.8\textwidth]{img/qrcode_murat.ambarkutuk.com.png}
    \end{block}

\end{column} % End of column 2.2

\end{columns} % End of the split of column 2

\end{column} % End of the second column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The third column
    \begin{alertblock}{NSF Style Summary}
        \begin{variableblock}{Contributions}{bg=skipper_smoke,fg=black}{bg=skipper_smoke,fg=black}
            The original contributions of this study are given below:
            \begin{enumerate}
                \item A framework that \alert{identifies uncertainty bounds} of vibro-localization results,
                \item A framework that handles \alert{multi-sensing} schemes to \alert{achieve minimal localization uncertainty},
                \item A framework that can be extended to localize events based on \alert{different natural phenomena} (sound, light, etc.),
                \item A framework that \alert{protects the privacy of the occupants}.
            \end{enumerate}
        \end{variableblock}
        \vspace{-2ex}
        \begin{variableblock}{Broader Impact}{bg=skipper_smoke,fg=black}{bg=skipper_smoke,fg=black}
            \begin{enumerate}
                \item \alert{Wild-life protection} by using songs of the animals,
                \item Gait analysis to \alert{diagnose} various \alert{neuro-degenerative diseases},
                \item \alert{Gun-shot localization} in city and urban neighborhoods,
                \item Supplementary information for \alert{indoor robots} for \alert{obstacle avoidance} and \alert{human-machine interaction},
                \item Improved accuracy and precision for \alert{acoustic cameras}.
            \end{enumerate}
        \end{variableblock}
        \vspace{-2ex}
        \begin{variableblock}{Intellectual Merit}{bg=skipper_smoke,fg=black}{bg=skipper_smoke,fg=black}
            \alert{Converting} the structures into \alert{agent}s and \alert{ability to} create a meaningful \alert{interaction with its occupants}. 
        \end{variableblock}
    \end{alertblock}
    \vspace{-2ex}
    \begin{block}{References}

        \nocite{*} % Insert publications even if they are not cited in the poster
        \small{\bibliographystyle{unsrt}
        \bibliography{cas-refs}\vspace{0.75in}}
    \end{block}
    \end{column} % End of the third column

\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}
