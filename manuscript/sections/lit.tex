\subsection{Visual Techniques}
With the latest advances in the Computer Vision field, marker-based and markerless visual \gls{ga} methods have been extensively explored in the literature.
The comprehensive review of such methods can be found in~\cite{Clark2013, Ceseracciu2014, Ong2017}.
These methods can be divided into two main camps based on the sensing technology behind them: \gls{tof} techniques and visual techniques.

Of all the available modalities, Kinect is one of the most prominent \gls{tof} sensors used in \gls{ga} studies because of its ability to provide the color images of the scene within its \gls{fov} as well as the corresponding depth map. 
Specifically, Kinect is a visual motion-tracking device where \gls{tof} information is used for each pixel to calculate the distance between the scene and the sensor.
By employing this dense color-distance information, various detection algorithms have been developed to identify different keypoints, i.e., different joint locations, on the human body.
Earlier techniques employed multiple Kinects placed orthogonally to calculate stride-to-stride gait variability as a fall risk indicator~\cite{Stone2011}.
Along with stride-to-stride variability, \citeauthor{Dolatabadi2014} demonstrated that the gait parameters related to stability and balance can be extracted from the skeletal data observed extended time windows~\cite{Dolatabadi2014}.
In their work, \citeauthor{Tupa2015} proposed another \gls{ga} technique that uses the skeletal information obtained from Kinect within a feed-forward neural network to estimate different gait features for the recognition of Parkinson's disease~\cite{Tupa2015}.
In order to omit the necessity for the training data required in neural networks, \citeauthor{Bei2018} posed the problem as a binary classification in an unsupervised manner to distinguish normal walking patterns from disorderly~\cite{Bei2018}.
Similarly, the wavelet transformation applied Kinect's skeletal data, specifically on the ankle locations, is shown to accurately identify gait phases in an unsupervised manner~\cite{Munoz2018}.

While Kinect provides benefits in \gls{ga} applications, it comes with its own shortcomings such as holes in the depth maps, and measurement noise in skeletal data, to name a few.
In order to tackle the shortcomings of such measurement techniques without losing the visual context, \citeauthor{Narayan2004} showed stereo vision techniques coupled with three-dimensional template matching yield accurate occupant recognition from their gaits.
On the other hand, \citeauthor{Sandau2014} demonstrated that gait kinematics can be obtained in the sagittal and frontal planes by reconstructing the observed scene and the gait by using patch-based multi-view stereo algorithms~\cite{Sandau2014}.
While achieving promising results, the proposed technique requires two types of calibrations, i.e., intrinsic and extrinsic calibrations, as well as a textured suit on the subjects to improve the three-dimensional reconstruction.
% intrinsic and extrinsic calibrations are aimed to characterize the optical parameters of the cameras and relative camera poses, respectively.
By employing such calibration techniques and the textured suit, the authors ensured increased accuracy in the reconstruction of the body segments.

\citeauthor{Li2019} addressed the gait analysis problem from multi-view stereo by using a frontoparallel camera setup.
Hence, the intrinsic and extrinsic parameters of the cameras can be reduced to three parameters: the focal length,  the pixel size of the cameras, and the physical distance between them.
In their framework, the joint locations in the stereo image pairs were detected with a deep neural network and successively converted to physical-space measurements with the reduced intrinsic and extrinsic parameters.
In order to eliminate the necessity of the intrinsic and extrinsic calibration, \citeauthor{Yagi2020} reframed the three-dimensional reconstruction problem to planar transformations where the transformation between the floor plane and the image plane are calculated with homography matrices, thereby, dismissing the calibration and the need for multiple cameras~\cite{Yagi2020}.
In their technique, the authors proposed rectifying the captured images such that the floor plane on the images becomes a bird-eye view after the homography transformations are applied.
The joint locations were first detected with a state-of-the-art convolutional neural network on the image plane, then transformed into physical space with the use of the calculated homography matrix.
Consequently, the proposed technique can be easily applied in less constrained environments such as homes, elder-care facilities, and rehabilitation facilities.

\subsection{Structural Vibration-based Techniques}
Along with the visual techniques, structural vibrations due to the subjects' gait patterns have drawn a significant amount of attention in society.
While the visual techniques can provide information about each joint individually, the vibration-based techniques are often focused on the determination of the heel-strike locations.
The vibration-based techniques often employ a number of accelerometers placed on the floor to capture the vibration induced on it. 

\gls{tdoa} is one of the most prominent localization technique which leverages the time-difference of the acceleration signal between the accelerometers.
Because of the material and geometric properties of the floor, the received vibration signal is often distorted.
\citeauthor{bahroun2014new} addressed this distorting nature of the propagation medium, i.e., the floor, by assuming the floor plane as a thin, damped, and dispersive plate to calculate different frequency-group propagation velocities for each sensor to account for inter-sensor differences in the impact localization~\cite{bahroun2014new}.

\citeauthor{poston2016indoor}, on the other hand, leverages an algorithm where \gls{toa} measurements are first used to condition the \gls{tdoa} technique.
Later in their work, the authors showed that the impact localization problem can be improved by considering the case where some of the sensors might be undergoing compression~\cite{poston2016indoor}.
\citeauthor{mirshekari2018occupant} considered the inaccuracies due to the distorted nature of the time-domain signal, and they moved the problem to the frequency-domain: the time domain signal obtained with accelerometers are first divided into its frequency components and adaptively selecting less-distorted components to localize the impact in the \gls{tdoa} framework~\cite{mirshekari2018occupant}.

In their experimental work, \citeauthor{kessler2019vibration} demonstrated that temporal gait parameters, i.e., cadence, cycle time, etc., can be obtained from the time-domain vibration measurements that are placed in a 115-feet hallway with significant accuracy~\cite{kessler2019vibration}.
As for the spatial gait parameters, \citeauthor{alajlouni2019new} proposed a heuristic step localization technique which formulates the heel-strike location as a linear combination between the sensor locations and their energy measurements of the time-domain measurements of the floor vibration signal~\cite{alajlouni2019new}.
The calibration-free nature of the proposed work provided great flexibility in the actual deployment due to its ease of deployment.
In their later work, \citeauthor{alajlouni2020passive} showed this localization technique can be improved by assuming the energy of vibration signal follows an exponential attenuation model~\cite{alajlouni2020passive} to calculate the heel-strike locations. 

All the aforementioned vibration techniques, however, reported sub-meter localization accuracy for the impact locations which makes it impractical to use them for \gls{ga}.
While there have been widely varying considerations of the \gls{ga} problem under different assumptions, postulations, and hypotheses, the authors realized the room for improvement for the \gls{ga} based on pure visual information such as (self-)occlusions, difficulties in calibration, etc.
In this paper, we propose a new \gls{ga} technique based on robust estimation technique to increase the ability of the impact localization with structural vibration signal observed by noisy accelerometers.
