\begin{thm}[Rayleigh's Signal Energy Theorem]
    \label{thm:rayleigh-deterministic}
    The Rayleigh's energy theorem states that the energy of a time domain continous and deterministic signal $y(t)$ is the integral of the square of magnitude of the signal.
    Equivalently, energy of the same signal can be obtained from its frequency-domain representation $Y(\omega)$.
    \begin{equation}
        e = \int_{-\infty}^{\infty} \left\lvert y(t) \right\rvert^{2} \diff{t} = \int_{-\infty}^{\infty} \left\lvert Y(\omega) \right\rvert^{2} \diff{\omega}
    \end{equation}

    Equivalently, the signal energy of a discrete time signal $y[k]$ is given by:

    \begin{equation}
        e = \sum_{k=-\infty}^{\infty} \left\lvert y[k] \right\rvert^2 = \sum_{k=-\infty}^{\infty} \left\lvert Y(\omega_k) \right\rvert^{2}
    \end{equation}
\end{thm}

\begin{thm}[Energy of a Stochastic Real Signal]
    \label{thm:rayleigh-stochastic}
    Akin to Rayleigh's theorem shown in~\Cref{thm:rayleigh-deterministic}, the signal energy of a discrete time domain stochastic signal $Y[k] \in \mathbb{R}$ can be obtained with the same manner.
    The integral is replaced with a summation which yields a cumulative random variable $E$.
    For the sake of brevity, we assume there is no correlation, i.e., independence, between random variables $Y[k]$ and $Y[s]$ for $\forall k = \{1,\ldots n \} \wedge k \neq s$.
    By employing Rayleigh's theorem, the energy random variable $E$ can be obtained as shown in \Cref{eq:rayleigh_prob}.
    \begin{equation}
        \label{eq:rayleigh_prob}
        E = \sum_{k=1}^{n} \left\lvert Y[k] \right\rvert^2 = \sum_{k=1}^{n} Y[k]^2 \sim \f{E},
    \end{equation}
    where $\f{E}$ represents the \gls{pdf} random variable $E$.
\end{thm}

\begin{thm}
    \label{thm:noncentral-chi}
    Squared sum of n number of nonstandard normally distributed random variables with unit variance follows the Noncentral Chi-squared distribution.

    
    Let $(X_1, \ldots, X_n)$ be normally distributed indepedent normal random variables with means  $\mu_i \neq 0$ and unit variances $\sigma_i^2 = 1$ for $i=\{1,\ldots, n\}$.
    Let $Y$ be the squared sum of the random variables $X_i$ as shown in the equation below.

    \begin{equation}
        Y = \sum_{i=1}^{n} X_i^2 \sim \chi^{\prime 2}_n \left(\lambda \right)
    \end{equation}
    
    The resulting random variable $Y$ is said to follow Noncentral Chi-squared distribution which denoted as $\chi^{\prime 2}_n \left(\lambda \right)$.
    In the short hand representation $\chi^{\prime 2}_n \left(\lambda \right)$, n represents the number of random variables used in the summation and is called degrees-of-freedom of the distribution, while $\lambda = \sum_{i=1}^{n} \mu_i^2$ represents the noncentrality parameter.
\end{thm}

\begin{thm}
    \label{thm:normal-approximation}
    Noncentral Chi-squared distribution converges to Normal distribution when $n$ or $\lambda$ is large.

    \begin{equation}
        Y \sim \chi^{\prime 2}_n \left(\lambda \right) \simeq \NSTD{n + \lambda}{2n+4\lambda}
    \end{equation}
\end{thm}

% \begin{thm}[Law of the unconscious statistician (LOTUS)]
%     \label{thm:lotus}
%     \gls{lotus} is a theorem employed to calculate the expected value of a function $g(X)$ of a random varible $X$ from $\f{X}$, i.e., the \gls{pdf} that is associated to $X$.

%     \begin{subequations}
%         If $X$ is a univariate continuous random variable: 
%         \begin{equation}
%             \expt{g(X)} = \int_{-\infty}^{\infty} g(x) \f{X} \, \diff{x}
%         \end{equation}

%         If $X$ is a univariate discrete random variable: 
%         \begin{equation}
%             \expt{g(X)} = \sum_{x} g(x) \f{X}
%         \end{equation}

%         If $\vect{X}$ is a multivariate continuous random variable such that $\vect{X} = \left(X_1, \ldots X_m \right)^\top \in \mathbb{R}^m$:
%         \begin{equation}
%             \expt{g(\vect{X})} = \int_{-\infty}^{\infty} \cdots \int_{-\infty}^{\infty} g(\vect{X}) \f{\vect{X}} \, \diff{x_1}\ldots\diff{x_m}
%         \end{equation}

%         If $\vect{X}$ is a multivariate discrete random variable such that $\vect{X} = \left(X_1, \ldots X_m \right)^\top \in \mathbb{R}^m$:
%         \begin{equation}
%             \expt{g(\vect{X})} = \sum_{x_m} \cdots \sum_{x_1} g(\vect{X}) \f{\vect{X}}
%         \end{equation}
%     \end{subequations}
% \end{thm}

\begin{thm}[Density Transformation Theorem]
    \label{thm:density}
    The density of functions of random variables can be calculated with the density transformation theorem.
    This theorem is essentially an extension of integration by substitution method.
    
    Let $\vect{X} = \left(X_1, \ldots X_m \right)^\top \in \mathbb{R}^m$ be an m-dimensional multivariate random variable with a joint \gls{cdf} of $\F{\vect{X}}=\prob{\vect{X}\leq \vect{x}}$ and joint \gls{pdf} of $\f{\vect{X}} = \dfrac{\partial}{\partial x_1} \cdots \dfrac{\partial}{\partial x_m} \F{\vect{X}}$.
    Assume $\vect{Y} = \left(Y_1, \ldots Y_n \right)^\top \in \mathbb{R}^n $ is another multivariate random variable defined as a function of $\vect{X}$, $\vect{Y}= \left(g_1\left( \vect{X} \right) \\  \ldots  \\ g_n\left(\vect{X} \right) \right)^\top = \vect{G}\left( \vect{X} \right)$, where $g_i: (\mathbb{X}_1, \ldots, \mathbb{X}_m) \subset \mathbb{R}^m \mapsto \mathbb{Y}_i \subset \mathbb{R}$ is an invertible multivariate continuous function.

    By the virtue of the relation between \gls{pdf}, we can show:
    \begin{subequations}
        \begin{align}
            \int_{\mathbb{Y}_1} \cdots \int_{\mathbb{Y}_n} &\f{\vect{Y}} \, \diff{y_1} \ldots \diff{y_n} = \int_{\mathbb{X}_1} \cdots \int_{\mathbb{X}_m} \f{\vect{X}} \, \diff{x_1} \ldots \diff{x_m} = 1 \\
            \intertext{Thus, \gls{pdf} of random variable $\vect{Y}$ can be obtained as given below:}
            &\f{\vect{Y}} = \dfrac{\partial}{\partial y_1} \cdots \dfrac{\partial}{\partial y_n} \int_{\mathbb{X}_1} \cdots \int_{\mathbb{X}_m} \f{\vect{X}} \, \diff{x_1} \ldots \diff{x_m}
        \end{align}
    \end{subequations}
    On the other hand, the \gls{cdf} of random variable $\vect{Y}$, can be shown as:
    \begin{subequations}
        \begin{align}
            F_{\vect{Y}}\left( \vect{y} \right) &= \prob{\vect{Y}\leq \vect{y}} = \prob{ \vect{X} \leq \vect{G}^{-1}(\vect{y})} \\
            F_{\vect{Y}}\left( \vect{y} \right) &= \int_{\vect{s}}^{\vect{y}} \f{\vect{Y}} \, \diff{\vect{y}} = \int_{\vect{G}^{-1}(\vect{s})}^{\vect{G}^{-1}(\vect{y})} \f{\vect{X}} \, \diff{\vect{x}}
        \end{align}
    \end{subequations}
    where $\vect{s} = (\sup{\mathbb{X}_1},\ldots, \sup{\mathbb{X}_m})^\top$.

    Now substitute $\vect{x} = \vect{G}^{-1}(\vect{u})$ in the integral on the right hand side, i.e., $\vect{x} = \vect{G}^{-1 }(\vect{u})$ and $\frac{\diff{\vect{u}}}{\diff{\vect{x}}}=\vect{G}^\prime\left(\vect{x}\right)$.
    Due to the inverse function theorem, we also have $\frac{\diff{\vect{x}}}{\diff{\vect{u}}}=\frac{\diff{\vect{G}^{-1}}}{\diff{\vect{u}}}$.
    Consequentially,
    \begin{subequations}
        \begin{align}
            \int_{\vect{s}}^{\vect{y}} \f{\vect{Y}} \, \diff{\vect{y}} = \int_{\vect{s}}^{\vect{y}} f_{\vect{X}}\left(\vect{G}^{-1}(\vect{u}) \right) \, \frac{\diff{\vect{G}^{-1}}}{\diff{\vect{u}}}\diff{\vect{u}}
        \end{align}
        Taking the derivative of both sides with respect to $\vect{y}$, we have:
        \begin{align}
            \f{\vect{Y}} &=  f_{\vect{X}}\left(\vect{G}^{-1}(\vect{y}) \right) \, \frac{\diff{\vect{G}^{-1}}}{\diff{\vect{y}}} \\
            &=  f_{\vect{X}}\left(\vect{G}^{-1}(\vect{y}) \right) \lvert \det{\vect{J}_{\vect{G}^{-1}}(\vect{y})} \rvert
        \end{align}
        where $\vect{J}_{\vect{G}^{-1}}(\vect{y})$ denotes the jacobian matrix of the inverse function $\vect{G}^{-1}$ evaluated at $\vect{y}$.
        Due to the implication of inverse function theorem and the properties of determinant operator, we have $\lvert \det{\vect{J}_{\vect{G}^{-1}}(\vect{y})}\rvert = \lvert\det{\inv{\vect{J}_{\vect{G}}}(\vect{y})}\rvert = \dfrac{1}{\lvert \det{\vect{J}_{\vect{G}}(\vect{x})} \rvert}$, assuming $\det{\vect{J}_{\vect{G}}(\vect{x})} \neq 0$, i.e., $\vect{G}(\vect{x})$ is continuously differentiable.
        \begin{equation}
            \f{\vect{Y}} = \frac{f_{\vect{X}}\left(\vect{G}^{-1}(\vect{y}) \right)}{\lvert \det{\vect{J}_{\vect{G}}(\vect{x})} \rvert}
        \end{equation}
    \end{subequations}
\end{thm}
