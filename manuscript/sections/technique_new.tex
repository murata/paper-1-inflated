
\input{figs/fig_interaction.tex}

\subsection{Localization Framework}
This section presents the core contribution of the paper, an energy-based vibro-localization technique that can accurately determine occupants' location in an indoor environment under measurement uncertainty.
This technique employs the vibro-measurements of a floor due to occupants' footfall patterns impacted on it.
As the occupant walks in the environment, the occupant's heel-strikes exert forcing, i.e., commonly known as \gls{grf}, on the floor generating structural vibration waves traveling in the floor.
\Cref{fig:overview} provides a graphical summary of the interaction between occupant (excitation), floor (wave propagation media), and the accelerometers (mode of perception).

Let $m$ number of floor-mounted single-axis accelerometers be located at $\vect{x}_{a_i} = \left(x_{a_i}, y_{a_i} \right)^\top \in \mathcal{S} \subset \mathbb{R}^2$ where subscript $i$ refers to the sensor index $\forall i \in \{1,\ldots,m\}$ in a rectangular localization space $\mathcal{S}$ defined as shown in \Cref{eq:S}.
\begin{equation}
    \mathcal{S} = \{ (x, y): \left(x_{min} \leq x \leq x_{max}\right) \wedge \left(y_{min} \leq y \leq y_{max}\right)\}
    \label{eq:S}
\end{equation}

In this work, we hypothesize that a localization function $\vect{h}_i: (e_i, \theta_i)^\top \mapsto \vect{x}_i$ that maps the signal energy $e_i$ of the $n$-dimensional vibro-measurement vector $\vect{z}_i = \left(z_i[1],\ldots,z_i[n] \right)^\top \in \mathbb{R}^n$ and the directionality of occupant $\theta_i$ to a location vector defined in localization space $\vect{x}_i \in \mathcal{S}$.
\Cref{eq:xi} demonstrates the hypothesized localization function $\vect{h}_i(e_i, \theta_i; \vect{\beta}_i)$.

\begin{equation}
    \vect{x}_i = \vect{h}_i\left(e_i, \theta_i; \vect{\beta}_i\right) = \vect{x}_{a_i} + g(e_i; \vect{\beta}_i) 
    \begin{bmatrix}
        \cos{\theta_i} \\
        \sin{\theta_i}
    \end{bmatrix} = \vect{x}_{true} + \vect{\chi}_i
    \label{eq:xi}
\end{equation}
where the vector $\vect{\chi}_i \in \mathbb{R}^2$ represents the estimation error when the occupant is located at $\vect{x}_{true}=\left(x_{true}, y_{true}\right)^\top \in \mathcal{S}$ while $\vect{\beta}_i$ represents a known calibration vector.
The ranging function $g\left(e_i; \vect{\beta} \right): e_i \mapsto d_i$ maps to energy of the vibration signal $e_i$ to the distance between the sensor and occupant $d_i$.
It is assumed to be a monotonic decreasing function for some positive energy measurement $e_i \in \mathbb{R}_+$; therefore, it is bijective and its inverse exists.
A graphical representation of \Cref{eq:xi} is shown in \Cref{fig:equation-graphical}.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=\textwidth]{figs/layout.eps}
    \caption{This figure demonstrates a localization case given in   \Cref{eq:xi}. The solid green arrow represents the true occupant location vector $\vect{x}_{true}$, while the dashed red arrow denotes the estimated range with respect to the $i^{th}$ accelerometer located at $\vect{x}_{a_i}$. As can be seen in the figure, localization error is represented as a random variable $\vect{x}_i$ whose mean and \%95 confidence interval is denoted with a red plus sign and a solid ellipse. In light of this representation, precision and accuracy of a localizer can be defined as the covariance and the magnitude of the localization error vector $\vect{\chi}_i$, respectively.}
    \label{fig:equation-graphical}
\end{figure}

In this framework, the occupant location with respect to the $i^{th}$ sensor is parameterized with its representation in polar coordinate system centered at sensor location $\vect{x}_{a_i}$.
In other words, the proposed framework has a flexibility to distinguish the distance $d_i$ and directionality $\theta_i$ components separately.
As an accelerometer by itself provides only ranging information, i.e., the distance between the occupant and itself, we assume directionality component $\theta_i$ is completely unknown.
Therefore, we can simply model it as random variable which follows a Uniform distribution between the range $(0, 2\pi)$, that is denoted as $\theta_i \sim \mathcal{U}(0, 2\pi)$.
% The location estimation in global coordinate system can be then represented as:

The defining properties of location estimate $\vect{x}_i$ can be obtained by studying its \gls{pdf} $\f{\vect{X}_i}$.
The derivation of the \gls{pdf} $\f{\vect{X}_i}$ given the \gls{pdf} of signal energy $\f{E_i}$ is a straight-forward application of Density Transformation Theorem shown in \Cref{thm:density}.
The following section provides a simple signal model with which the signal energy $e_i$ can be computed from the measurement vector $\vect{z}_i$.

\subsection{Signal Uncertainty Model and Derivation of Signal Energy $e_i$ Under Uncertainty}
As shown in \Cref{thm:rayleigh-deterministic} and \Cref{thm:rayleigh-stochastic}, Rayleigh's theorem can be invoked to obtain the signal energy $e_i$ from the measurement vector $\vect{z}_i$.
Without losing the generality, we model the measurement vector $\vect{z}_i$ as the composition of the unknown true measurement vector $\vect{z}_{true, i}$ and the disturbance vector $\vect{\zeta}_i$ as given in \Cref{eq:zi}.

\begin{equation}
    \vect{z}_i = \vect{z}_{true, i} + \vect{\zeta}_i
    \label{eq:zi}
\end{equation}
where $\vect{z}_{true, i} = \left(z_i[1],\ldots, z_i[n]\right)^\top$ and $\vect{\zeta}_i = \left(\zeta_i[1], \ldots, \zeta_i[n]\right)^\top$.
It is quite common to see the disturbance vector $\vect{\zeta}_i$ as Normally distributed \texttt{iid}\footnote{Indepedently and identically distributed} random variables, i.e., $\zeta_i[k] \sim \N{\mu_\zeta}{\sigma_\zeta}$.
In this study, we found that the signal energy $e_i \sim \f{E_i} = \chi^{\prime, 2}_n \left( \lambda \right)$ exactly follows a Non-central Chi-squared Distrubtion with $n$ degrees-of-freedom (number of samples that the energy is calculated with) and noncentrality parameter $\lambda$, (c.f. \Cref{thm:noncentral-chi}).
With this, we present our first theorem showing $\f{E_i}$ can be safely approximated with a Normally distributed random variable.

\begin{thm}[Energy distribution of a stochastic representation of accelerometer measurements converges safely Normal distribution.]
    \label{thm:opus-theorem}
    Following Rayleigh's theorem (cf. \Cref{thm:rayleigh-deterministic} in appendices), the signal energy of a discrete time stochastic signal can be calculated as shown in \Cref{thm:rayleigh-stochastic}.
    As the random variables in hand are normally distributed, we can safely employ \Cref{thm:noncentral-chi} to obtain the signal energy distribution of a stochastic signal as Noncentral Chi-squared distribution.
    However, it should be noted that Noncentral Chi-squared distribution is known to converge to a Normal distribution when number of summands are sufficiently large ($n>20$) and summands are independent.
    As the modern accelerometers can provide thousands of samples in a second, we assert this assumption holds and the energy distribution safely converges the Normal distribution.
    The details of this assertion can be seen in Proof~\ref{pro:energy-normal}.
\end{thm}

\begin{pf}[Proof of \Cref{thm:opus-theorem}]
    \label{pro:energy-normal}
    Applying the energy formula, we can
    \begin{subequations}
        \begin{align}
            E &= \sum_{k=1}^{n} \left\lvert Z[k] \right\rvert^{2} = \sum_{k=1}^{n} Z[k]^{2} 
            \intertext{If we multiply and divide the summands with the variance $\sigma_\zeta^2$, we have:}
            E &= \sum_{k=1}^{n} \frac{\sigma_\zeta^2}{\sigma_\zeta^2} Z[k]^2  = \sum_{k=1}^{n} \sigma_\zeta^2 \tilde{Z}[k]^{2}\text{, where } \tilde{Z}_k \sim \NSTD{\frac{z_t[k] + \mu_\zeta}{\sigma_\zeta}}{1} \\
            E &= \sigma_\zeta^2 \sum_{k=1}^{n} \tilde{Z}[k]^{2} \label{eq:energy_probablistic}
        \end{align}
    \end{subequations}
    Realize \Cref{eq:energy_probablistic} suggests that the energy is the squared sum of $n$ normally distributed random variables $\tilde{Z}[k]$ that have unit variances.
    Thus, we can show that $\dfrac{E}{\sigma_\zeta^2} \sim \chi^{\prime 2}_n\left(\lambda\right)$ follows with n degrees-of-freedom $\lambda=\sum\limits_{k=1}^{n} \left( \dfrac{z_t[k] + \mu_\zeta}{\sigma_\zeta} \right)^2$ (cf. \Cref{thm:noncentral-chi}).

    Without losing generality, we can develop on this idea:
    \begin{subequations}
        \begin{align}
            \frac{E}{\sigma^2_\zeta} &\sim \chi^{\prime 2}_n \left( \sum_{k=1}^n \left( \dfrac{z_t[k] + \mu_\zeta}{\sigma_\zeta}\right)^2 \right) \sim \chi^{\prime 2}_n \left( \frac{1}{\sigma^2_\zeta} \sum_{k=1}^n \left(z_t[k] + \mu_\zeta\right)^2 \right) 
            \intertext{By invoking \Cref{thm:normal-approximation}, it can be showed that the left-hand side of the equation can be actually approximated to a Normally distributed random variable:}
            \frac{E}{\sigma^2_\zeta} &\simeq \NSTD{n + \frac{1}{\sigma^2_\zeta} \sum_{k=1}^n \left( z_t[k] + \mu_\zeta\right)^2}{2n+\frac{4}{\sigma^2_\zeta} \sum_{k=1}^n \left( z_t[k] + \mu_\zeta \right)^2}
        \end{align}
        \label{eq:chisq}
    \end{subequations}
    Finally, the scalibility property of Normal distribution can be employed to derive the distribution of the signal energy $E \simeq \N{\mu_E}{\sigma_E}$, where the mean and variance is characterized as shown below. 

    \begin{subequations}
        \begin{align*}
            \mu_E &= \expt{E} = \sigma^2_\zeta n + \sum_{k=1}^n \left( z_t[k] + \mu_\zeta \right)^2,
            \intertext{and}
            \sigma_E^2 &= \Var{E} = 2\sigma^4_\zeta n+4 \sigma^2_\zeta \sum_{k=1}^n \left( z_t[k] + \mu_\zeta \right)^2
        \end{align*}
    \end{subequations}
\end{pf}

As a consequence of \Cref{thm:opus-theorem}, we present the next corollary:

\begin{cor}
    If the sensor has calibrated such that sensor bias is neglibly small, i.e. $\mu_\zeta  \approxeq 0$ for $k=\{1, \ldots, n \}$ and the variance of the measurement error is known $\sigma_\zeta^2$, the energy distribution can be parameterized with number of samples $n$ and the unknown true energy $e_t = \sum\limits_{k=1}^n z_t[k]^2$ that the sensor was supposed to register: 
    \begin{equation}
        \boxed{
            E \sim f_{E}(e; e_t) \simeq \NSTD{n \sigma_\zeta^2 + e_t}{2 n \sigma_\zeta^4 + 4 e_t \sigma_\zeta^2} = \dfrac{1}{\sigma_E \sqrt{2\pi}}\exp{\left[-\frac{\left(e-\mu_E\right)^2}{2\sigma^2_E} \right]}
        }
    \end{equation}
    where the mean $\mu_E$ and variance $\sigma_E^2$ of the energy distribution are given by $\mu_E = \sigma_\zeta^2 + e_t$, $\sigma_E^2 = 2 n \sigma_\zeta^4 + 4 e_t \sigma_\zeta^2$, respectively.
\end{cor}

\subsection{Derivation of \gls{pdf} of Location Estimation $\f{\vect{X}_i}$}
Given the localization function $\vect{h}_i(e_i, \theta_i; \vect{\beta}_i)$ that maps $(e_i, \theta_i)^\top \mapsto \vect{x}_i$, we can finally obtain the \gls{pdf} of the location estimate $\vect{x}_i$ by invoking Density Transformation theorem presented in \Cref{thm:density}.

\begin{subequations}
    \label{eq:fxi}
    \begin{align}
        f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\beta}\right) &= f_{E_i, \theta_i}\left( g^{-1}\left(\norm{\vect{x}_i-\vect{x}_{a_{i}}} \right), \theta_i; e_{true,i}, \vect{\beta}_i \right) \left\lvert \det{\vect{J}_{\vect{h_i}^{-1}}(\vect{x}_i; \vect{\beta}_i)}\right\rvert
        \intertext{because $E_i$ and $\theta_i$ are independent from each other:}
        &= f_{E_i}(g^{-1}\left(\norm{\vect{x}_i-\vect{x}_{a_{i}}} \right); e_{true,i}, \vect{\beta}_i) \f{\theta_i} \left\lvert \det{\vect{J}_{\vect{h_i}^{-1}}(\vect{x}_i; \vect{\beta}_i)}\right\rvert
        \intertext{where $\f{\theta_i} = \frac{1}{2\pi}$ and $f_{E_i}(e; e_{true,i}, \vect{\beta}_i) = \frac{1}{\sigma_E \sqrt{2\pi}}\exp{\left[-\frac{\left(e-\mu_E\right)^2}{2\sigma^2_E} \right]}$}
        \f{\vect{X}_i} &= \frac{1}{\sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g^{-1}\left(\norm{\vect{x}_i-\vect{x}_{a_{i}}} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]} \left\lvert \det{\vect{J}_{\vect{h_i}^{-1}}(\vect{x}_i; \vect{\beta}_i)}\right\rvert
    \end{align}
\end{subequations}
where $\vect{J}_{\vect{h_i}^{-1}}(\vect{x}_i; \vect{\beta}_i)$ denotes the Jacobian matrix of inverse localization function $\vect{h_i}^{-1}(\cdot)$ evaluated at $\vect{x_i}$.

The definition of the Jacobian matrix $\vect{J}_{\vect{h_i}^{-1}}(\vect{x}_i)$ is given in \Cref{eq:jacobian}.
\begin{equation}
    \label{eq:jacobian}
    \vect{J}_{\vect{h_i}^{-1}}(\vect{x}) = \begin{bmatrix}
        \frac{\partial g^{-1}}{\partial x}\left(\norm{\vect{x}-\vect{x}_{a_i}}_2; \vect{\beta}_i \right) & \frac{\partial g^{-1}}{\partial y}\left(\norm{\vect{x}-\vect{x}_{a_i}}_2; \vect{\beta}_i \right) \\
        \frac{\partial \angle{\left(\vect{x}-\vect{x}_{a_i}\right)}}{\partial x} & \frac{\partial \angle{\left(\vect{x}-\vect{x}_{a_i}\right)}}{\partial y}
    \end{bmatrix}
\end{equation}
where 
\begin{equation*}
    \det{\vect{J}_{\vect{h_i}^{-1}}(\vect{x})} = \frac{1}{\norm{\vect{x} - \vect{x}_{a_i}}}\frac{\partial g^{-1}}{\partial \vect{x}}\left( \norm{\vect{x} - \vect{x}_{a_{i}}}\right)
\end{equation*}

By plugging \Cref{eq:jacobian} into \Cref{eq:fxi}, we obtain:

% \begin{equation*}
%     f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\beta}\right) = \frac{1}{\sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g^{-1}\left(\norm{\vect{x}_i-\vect{x}_{a_{i}}} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]} \left\lvert \det{\begin{bmatrix}
%         \frac{\partial g^{-1}}{\partial x}\left(\norm{\vect{x}-\vect{x}_{a_i}}_2; \vect{\beta}\right) & \frac{\partial g^{-1}}{\partial y}\left(\norm{\vect{x}-\vect{x}_{a_i}}_2; \vect{\beta}\right) \\
%         \frac{\partial \angle{\left(\vect{x}-\vect{x}_{a_i}\right)}}{\partial x} & \frac{\partial \angle{\left(\vect{x}-\vect{x}_{a_i}\right)}}{\partial y}
%     \end{bmatrix}} \right\rvert
% \end{equation*}

\begin{equation}
    \boxed{
        \Rightarrow f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\beta}\right) = \frac{\left\lvert \frac{\partial g^{-1}}{\partial \vect{x}_i}\left( \norm{\vect{x}_i - \vect{x}_{a_{i}}}\right) \right\rvert}{ \norm{\vect{x}_i - \vect{x}_{a_{i}}} \sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g^{-1}\left(\norm{\vect{x}_i-\vect{x}_{a_{i}}} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]}
    }
    \label{eq:fxi_final} 
\end{equation}

\Cref{eq:fxi_final} shows the \glspl{pdf} $f_{\vect{X}_i}(\cdot)$ assign a probability value for an occupant located at vector $\vect{x}_i$ given the inverse of the ranging function $g(\cdot)$ and sensor location $\vect{x_{a_i}}$.
Notice the term in the denominator, i.e., $\norm{\vect{x}_i - \vect{x}_{a_i}}$, in \Cref{eq:fxi_final} resulting in an negative relationship between the probability values and the distance between the sensor and the impact location.  
% As can be seen in the The \gls{pdf} $f_{\vect{X}_i}$ encodes

\subsection{Multi-Sensor Perception \& Data Fusion}
Given the \glspl{pdf} $f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\beta}\right)$ for all sensors with index $i \in \{1, \ldots, m\}$, we seek a joint-\gls{pdf}.
As all the sensors' are independent from each other, we can represent the joint-\gls{pdf} as given below:

\begin{equation}
    f_{\vect{X}_1, \ldots, \vect{X}_m}\left(\vect{x}_1, \ldots, \vect{x}_m; e_{true, 1}, \ldots, e_{true, m}, \vect{\beta}\right) = \prod_{i=1}^{m} f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\beta}\right)
\end{equation}

Let $\kappa_i = \dfrac{e_{true,i}}{e_i}$ be an indepedent variable denoting the ratio between the unknown true energy $e_{true, i}$ and measured signal energy $e_i$.
Notice that $e_i = e_{true, i} + \epsilon_i$; thus, $\kappa_i \in [0, 1]$.
Given this definition, we can reparameterize the joint-\gls{pdf} as given below:

\begin{equation}
    f_{\vect{X}_1, \ldots, \vect{X}_m}\left(\vect{x}_1, \ldots, \vect{x}_m; \kappa_1, \ldots, \kappa_m, \vect{\beta}\right) = \prod_{i=1}^{m} f_{\vect{X}_i}\left(\vect{x}_i; \kappa_i, \vect{\beta}\right)
\end{equation}

Given this representation we can define a cost function $J(\vect{x}, \vect{\kappa})$ to minimize as a function $0\leq\kappa_i\leq1$: 

\begin{subequations}
    \begin{align}
        J\left(\vect{x}, \vect{\kappa} \right) &= - \sum_{i=1}^{m} \log{f_{\vect{X}_i}\left(\vect{x}; \kappa_i, \vect{\beta}\right)} + \alpha \sum_{i=1}^{m} e_i - e_{true, i} \\
        &= - \sum_{i=1}^{m} \log{f_{\vect{X}_i}\left(\vect{x}; \kappa_i, \vect{\beta}\right)} + \alpha \sum_{i=1}^{m} e_i - \kappa_i e_i \\
        J\left(\vect{x}, \vect{\kappa} \right) &= - \sum_{i=1}^{m} \log{f_{\vect{X}_i}\left(\vect{x}; \kappa_i, \vect{\beta}\right)} + \alpha \sum_{i=1}^{m} e_i (1-\kappa_i)
    \end{align}
\end{subequations}

The optimization problem is then in hand can be stated as given in \Cref{eq:optimization}.
\begin{equation}
    \label{eq:optimization}
    \vect{x}^* = \argmin_{\vect{x}, \vect{\kappa}} J\left(\vect{x}, \vect{\kappa}\right)
\end{equation}

Because the nonlinear system of equations shown in \Cref{eq:optimization} is under-determined ($m$ equations, $m+2$ parameters to solve), we employ an iterative solution technique, Gradient Descent, to solve it.
% \Cref{alg:cap} shows the algorithm developed based on Gradient Descent to solve for optimization problem in hand.

% \begin{algorithm}[htb!]
% \caption{Guided Gradient Descent-based Multi-Sensor Localization Algorithm}\label{alg:cap}
% \begin{algorithmic}
% \Require $\vect{n} \geq 0$
% \Ensure $y = x^n$
% \State $y \gets 1$
% \State $X \gets x$
% \State $N \gets n$
% \While{$N \neq 0$}
% \If{$N$ is even}
%     \State $X \gets X \times X$
%     \State $N \gets \frac{N}{2}$  \Comment{This is a comment}
% \ElsIf{$N$ is odd}
%     \State $y \gets y \times X$
%     \State $N \gets N - 1$
% \EndIf
% \EndWhile
% \end{algorithmic}
% \end{algorithm}
