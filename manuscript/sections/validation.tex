The efficacy and performance of the proposed technique have been evaluated with a comprehensive set of parametric studies based on controlled experiments.
In this section, the details of the experiments are provided.

\subsection{Experiments}
The evaluate the performance of the proposed vibro-localization technique, a set of controlled experiments were conducted.
In these experiments, two occupants were asked to walk on a predefined path placed on a 16 meters stretch of a long hallways.
This data was also employed in different studies {\color{red}{CITATIONS!}} to benchmark different approaches.
For the sake of continuity, cohesion, and relevancy, we chose our baseline from these studies and employed the same data obtained in the experiments conducted in these studies. 
The sensor layout and walking path are shown in \Cref{fig:layout-goodwin}.
The sensors are mounted on the I-beams beneath a concrete floor on the $4^{th}$ of Goodwin Hall, Virginia Tech.
As can be seen in \Cref{fig:layout-goodwin}, the closest 11 accelerometers were used to record the structural vibration data of 162 steps per occupant (324 steps in total).
Interested readers are refered to the original study for the technical details of the experiments {\color{red}{CITATION}}.

\begin{figure}
    \centering
    \includegraphics{example-image-a}
    \caption{Walking paths on the 4th level of Goodwin Hall. Each test subject traversed each of the three horizontal paths twice, once in each direction. The
    prescribed footstep locations (black circles) and sensor locations (green squares) are also shown. Adapted from {\color{red}{CITATION}}.}
    \label{fig:layout-goodwin}
\end{figure}

\subparagraph{Data and Model Validity:} In the initial study, the researchers collected vibration measurements during a period of low activity when there was no movement in or around the instrumented hallway.
The findings indicated that the noise profiles of the sensors followed a normal distribution with a mean of zero and a consistent variance across all sensors.
As a result, we propose that the signal model presented \Cref{eq:zi} is a valid representation of the vibration measurements. 
Based on this observation, we confidently assert that the random variables associated with the energy of the signal, denoted as $e_i$ for $i=\{1,\ldots,m\}$, accurately reflect the experimental data.

\subparagraph{Data and Model Difference from the Original Study:} During the data collection process, the data acquisition system remained active while occupants walked along their designated paths.
Consequently, it became necessary to employ signal detection and clipping techniques to calculate the energy of each individual step.
The distinction between the original study and this study lies in the methodology used to detect these steps before applying localization techniques.
Our study implemented an in-house developed stochastic detection algorithm, which yielded different signal energy values compared to the original study.
While the original study employed a tight-window approach that overlapped the vibration response with \gls{grf}, our detection algorithm adopted a more relaxed approach.
This means that there were periods of ``silence`'' before the vibration actually commenced and after it subsided.

This difference can be seen in \Cref{fig:detection}.
\begin{figure}[htb!]
    \centering
    \begin{subfigure}{0.4\textwidth}
        \centering
        \includegraphics[width=\linewidth]{example-image-a}
        \caption{}
        % \label{}
    \end{subfigure}
    ~
    \begin{subfigure}{0.4\textwidth}
        \centering
        \includegraphics[width=\linewidth]{example-image-b}
        \caption{}
        % \label{}
    \end{subfigure}
    \caption{}
    \label{fig:detection}
\end{figure}

An important aspect that we would like to emphasize to the readers is that the baseline results presented in {\color{red}{CITATIONS!}} were defined after applying a Kalman Filter update step to the localization results.
As a result, the localization results reported in the original work were relatively smaller compared to the findings presented in this document. 

\subsection{Results}
In this subsection, we provide the results of the detailed analyses conducted in our study.
As mentioned earlier, the work published in {\color{red}{CITATION}} is chosen to be the baseline.
Furthermore, we evaluated the proposed technique under two different scenarios: with complete lack of directional information, and with some directional information.
The complete lack of directional information is formulated in \Cref{eq:fxi_final} and will be referred as Proposed-1 in the remainder of the document.
The second scenario is created by imposing the directionality of the occupant location with respect to each sensor. 

\begin{equation*}
    \theta_i \sim \N{\theta_{true, i}}{\left(\frac{\pi}{12}\right)}
\end{equation*} 
where the true direction $\theta_{true, i}$ represents the angle between the sensor location vector $\vect{x}_{a_i}$ and the true occupant location vector $\vect{x}_{true}$ for all sensors indexed with $i$.
While such information cannot be derived from the energy of vibro-measurements, it can be pursued with the time-domain measurements $z_i[k]$.
For the sake of brevity, this scenario will be referred as Proposed-2.

\begin{figure}[tbh!]
    \centering
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/occupant-1/proposed-x.eps}
        \caption{Occupant-1 data in $x-$direction}
        \label{fig:est-x-results-occupant-1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/occupant-2/proposed-x.eps}
        \caption{Occupant-2 data in $x-$direction}
        \label{fig:est-x-results-occupant-2}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/occupant-1/proposed-y.eps}
        \caption{Occupant-1 data in $y-$direction}
        \label{fig:est-y-results-occupant-1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/occupant-2/proposed-y.eps}
        \caption{Occupant-2 data in $y-$direction}
        \label{fig:est-y-results-occupant-2}
    \end{subfigure}
    \caption{Localization results in $x-$ and $y-$ direction demonstrated versus the true $x-$ and $y-$values. The horizontal axis represents the true $x-$ and $y-$ values while the vertical axis denotes the result of the vibro-localization techniques.
    In all the plots, blue cross marks represent the proposed technique with lack of directional information while the orange plus marks denote the proposed technique with some directional information injected into the technique.
    Furthermore, the yellow dots mark the baseline results.
    % \textbf{Top Left:} The results obtained from the first occupant's data.
    % \textbf{Top Right:} The results obtained from the first occupant's data.
    % \textbf{Bottom Left:} The results obtained from the first occupant's data.
    % \textbf{Bottom Right:} The results obtained from the first occupant's data.
    }
    \label{fig:est-results}
\end{figure}

\Cref{fig:est-results} demonstrate the estimate localization parameters $x-$ and $y-$ direction versus the true values in the dataset.
\Cref{fig:est-x-results-occupant-1,fig:est-x-results-occupant-2} shows the results in $x-$direction obtained from the baseline and the proposed technique for the first and second occupant, respectively.
As can be seen in the figures, the proposed technique closely follows the true $x-$ values while tracking both of the occupants.
While the original technique, i.e., the Proposed-1, tracks the true values, there were some degradation at the two tails of the true values.
By injection the directional information, we observed a significant increase in the tracking capabilities of the proposed technique, denoted as the Proposed-2, at the tails.
An interesting observation can be made for the tracking performance of the baseline and the proposed technique in $y-$direction: the baseline and the Proposed-1 techniques displayed a significantly different tracking performance.
This behavior can be explained with the limited range in the dataset in the $y-$ direction.
On the other hand, Proposed-2 seems to be adapt this limited range.

\begin{table}[htb!]
    \centering
    \scriptsize
    \begin{tabular}{@{} l cc cc cc cc cc cc @{}} \\ \toprule
        & \multicolumn{12}{c}{Occupant-1} \\
        \cmidrule(lr){2-13}
        Metric: & Mean & $\Delta$ & Std. Dev. & $\Delta$ & Median & $\Delta$ & RMS & $\Delta$ & Min & $\Delta$ & Max & $\Delta$ \\  
        Unit: & [m] & \% & [m] & \%  & [m] & \%  & [m] & \%  & [m] & \%  & [m] & \%  \\
        \cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(lr){6-7} \cmidrule(lr){8-9} \cmidrule(lr){10-11} \cmidrule(lr){12-13}
        Baseline & 1.85 & -- & \textbf{1.08} & -- & 1.79 & -- & 2.14 & -- & 0.18 & -- & 7.46 & -- \\
        Proposed-1 & 1.60 & -13.51 & 1.57 & 45.37 & 1.06 & -40.78 & 2.24 & 4.67 & 0.02 & -88.89 & \textbf{7.39} & -0.94 \\
        Proposed-2 & \textbf{0.82} & -55.68 & 1.21 & 12.04 & \textbf{0.62} & -65.36 & \textbf{1.46} & -31.78 & \textbf{0.01} & -94.44 & 14.09 & 88.87 \\ \midrule

        & \multicolumn{12}{c}{Occupant-2} \\
        \cmidrule(lr){2-13}
        & Mean & $\Delta$ & Std. Dev. & $\Delta$ & Median & $\Delta$ & RMS & $\Delta$ & Min & $\Delta$ & Max & $\Delta$ \\  
        & [m] & \% & [m] & \%  & [m] & \%  & [m] & \%  & [m] & \%  & [m] & \%  \\
        \cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(lr){6-7} \cmidrule(lr){8-9} \cmidrule(lr){10-11} \cmidrule(lr){12-13}
        Baseline & 1.79 & -- & 1.12 & -- & 1.61 & -- & 2.11 & -- & 0.11 & -- & 5.9 & -- \\
        Proposed-1 & 1.59 & -11.17 & 1.74 & 55.36 & 1.00 & -37.89 & 2.35 & 10.21 & 0.01 & -90.91 & 10.73 & 81.86 \\
        Proposed-2 & \textbf{0.66} & -63.13 & \textbf{0.56} & -50.00 & \textbf{0.51} & -68.32 & \textbf{0.87} & -52.7 & \textbf{0.00} & -- & \textbf{2.99} & -49.32 \\ \bottomrule
    \end{tabular}
    \caption{Descriptive statistics of the normed localization error for occupant one \& two.}
    \label{tab:sensors-occupant}
\end{table}

In order to evaluate the performance of the proposed technique, we conducted a quantitatively analyses on the normed localization error.
\Cref{tab:sensors-occupant} tabulates the normed localization error under different error metrics for each occupant's data.
The metrics listed in \Cref{tab:sensors-occupant} can be used to define the descriptive statistics of the localization error made by each technique.
The results of the controlled experiments show that baseline yielded relatively higher localization error than the proposed technique in almost all metrics.
It should be noted that the baseline yielded localization errors such that the standard deviation is relative smaller than the proposed technique in the first occupant's data.
This phenomenon can be explained by the presence of the outliers in the localization estimates in the proposed technique.

\begin{figure}[tbh!]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-1/violinplot.eps}
        \caption{Occupant-1}
        \label{fig:result-box1}
    \end{subfigure}
    ~
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-2/violinplot.eps}
        \caption{Occupant-2}
        \label{fig:result-box2}
    \end{subfigure}
    \caption{This figure demonstrates the box-whiskers plot of the normed localization error made by each technique. \textbf{Left:} The localization error obtained from the first occupant's data. The figure shows Proposed-1 and Proposed-2 achieves lower median localization error. Also, it should be noted that the result obtained from }
    \label{fig:boxplots}
\end{figure}

A more nuanced way of representing the error characteristic is given in the box plots shown in \Cref{fig:result-box1,fig:result-box2}.
{\color{red}{MORE EXPLANATION IS NEEDED HERE FOR THE BOX WHISKERS}}.
As can be seen in the aferomentioned figures, the baseline seem to generate more consistent results where the proposed algorithm generated some outlying results based on the rank statistics obtained from the normed localization error vector of each technique.
In other words, the exact \gls{pdf} of the error metric of the proposed technique should be more positively skewed than that of the baseline.
This notion is investigated later by analyzing the empirical \gls{pdf} of the normed localization error vectors.
Furthermore, the outlying location estimated obtained by the proposed technique can be seen on the empirical-\glspl{pdf}.
These outliers create smaller peaks at the right tail moving the mean localization error towards them.

\begin{figure}[bth!]
    \centering
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-1/epdf.eps}
        \caption{}
        \label{fig:result-pdf-occupant-1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/occupant-2/epdf.eps}
        \caption{}
        \label{fig:result-pdf-occupant-2}
    \end{subfigure}
    \caption{}
    \label{fig:results-pdf}
\end{figure}

\Cref{fig:results-pdf} shows a empirical-\glspl{pdf} of the normed localization error obtained with the location estimates.
As can be seen in~\Cref{fig:result-pdf-occupant-1}, the baseline yielded localization errors that follow a quasi-uniform~\gls{pdf}, while the localization errors observed with the proposed technique follows a decaying nature.
In other words, majority of the errors were smaller than the baseline.
Similar trends can be seen in the second occupant's results, c.f. \Cref{fig:result-pdf-occupant-2}. 
\begin{figure}[htb!]
    \centering
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-1/ecdf.eps}
        \caption{}
        \label{fig:result-cdf-occupant-1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/occupant-2/ecdf.eps}
        \caption{}
        \label{fig:result-cdf-occupant-2}
    \end{subfigure}
    \caption{}
    \label{fig:results-cdf}
\end{figure}

\Cref{fig:results-cdf} demonstrates \glspl{cdf} of the normed localization error obtained from the baseline and the proposed vibro-localization technique. 
\Cref{fig:result-cdf-occupant-1,fig:result-cdf-occupant-2} show the empirical-\gls{cdf} obtained when only the first and second occupant's data is used, respectively.
While the baseline technique performed consistently on both datasets, the proposed algorithm seems to
\lipsum[1]

\begin{table}[htb!]
    \centering
    \footnotesize
    \begin{tabular}{@{}l ccc ccc @{}} \\ \toprule
        & \multicolumn{3}{c}{Occupant-1} & \multicolumn{3}{c}{Occupant-2} \\ 
         \cmidrule(lr){2-4} \cmidrule(lr){5-7}
        & Baseline & Proposed-1 & Proposed-2 & Baseline & Proposed-1 & Proposed-2  \\ \midrule
        60\% & 2.1042 & 1.2926 & \textbf{0.7816} & 2.0414 & 1.3828 & \textbf{0.6313} \\ 
        70\% & 2.4349 & 1.7435 & \textbf{0.9616} & 2.2846 & 1.6533 & \textbf{0.8417} \\ 
        80\% & 2.6453 & 2.8557 & \textbf{1.1423} & 2.6754 & 2.4048 & \textbf{1.0822} \\ 
        90\% & 3.0962 & 4.0882 & \textbf{1.6232} & 3.1864 & 3.1864 & \textbf{1.3527} \\ \bottomrule
    \end{tabular}
    \caption{The cumulative probability values measured at various confidence levels.}
    \label{tab:cumulative}
\end{table}

\lipsum[1]

\begin{figure}[tbh!]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-1/average_information-proposed-1.eps}
        \caption{Occupant-1 Proposed-1}
        \label{fig:result-information-proposed-1}
    \end{subfigure}
    ~
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-1/average_information-proposed-2.eps}
        \caption{Occupant-1 Proposed-2}
        \label{fig:result-information-proposed-2}
    \end{subfigure}
    ~
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-2/average_information-proposed-1.eps}
        \caption{Occupant-2 Proposed-1}
        \label{fig:result-information-proposed-1}
    \end{subfigure}
    ~
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-2/average_information-proposed-2.eps}
        \caption{Occupant-2 Proposed-2}
        \label{fig:result-information-proposed-2}
    \end{subfigure}
    \caption{Step Index vs. Joint-Information}
    \label{fig:results-information}
\end{figure}

\lipsum[1]

\begin{figure}[htb!]
    \centering
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/occupant-1/entropy-error.eps}
        \caption{Occupant-1}
        % \label{fig:result-cdf-occupant-1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figs/occupant-2/entropy-error.eps}
        \caption{Occupant-2}
        % \label{fig:result-cdf-occupant-2}
    \end{subfigure}
    \caption{Information vs. Localization Error}
    % \label{fig:results-cdf}
\end{figure}
